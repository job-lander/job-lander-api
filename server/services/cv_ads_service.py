from data.database import (
    read_query,
    _get_connection,
    insert_query_transactional,
    update_query,
)
from data.models.common_models import CreatedLocation, CreatedSkill, Status
from data.models.professional_models import CreatedCVAd, CvAd, CVAdResponseModel


def create_cv_ad(
    cv_ad: CreatedCVAd,
    location: CreatedLocation,
    skills: list[CreatedSkill],
    professional_id: int,
    insert_data_cv_func=None,
    insert_country_func=None,
    insert_town_func=None,
    get_location_id_func=None,
    insert_skill_func=None,
):
    try:
        conn = _get_connection()

        if insert_country_func is None:
            insert_country_func = insert_query_transactional
        country_id = insert_country_func(
            """INSERT into countries SET name = ? ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (location.country,),
        )

        if insert_town_func is None:
            insert_town_func = insert_query_transactional
        town_id = insert_town_func(
            """INSERT into towns SET name = ? ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (location.town,),
        )

        if get_location_id_func is None:
            get_location_id_func = insert_query_transactional
        location_id = get_location_id_func(
            """INSERT into locations (town_id, country_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (
                town_id,
                country_id,
            ),
        )

        if insert_data_cv_func is None:
            insert_data_cv_func = insert_query_transactional
        generated_id = insert_data_cv_func(
            """
            INSERT INTO cv_ads 
            (title, short_desc, salary_low, salary_high, is_remote, professional_id, status_id, locations_id) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?)
            """,
            conn,
            (
                cv_ad.title,
                cv_ad.short_desc,
                cv_ad.salary_low,
                cv_ad.salary_high,
                cv_ad.is_remote,
                professional_id,
                Status.UNAPPROVED,
                location_id,
            ),
        )

        for skill in skills:
            if insert_skill_func is None:
                insert_skill_func = insert_query_transactional
            generated_skill_id = insert_query_transactional(
                """INSERT INTO skills (name, status_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
                conn,
                (
                    skill.name,
                    Status.UNAPPROVED,
                ),
            )
            if skill.level == "beginner":
                skill_level_id = 1
            elif skill.level == "intermediate":
                skill_level_id = 2
            elif skill.level == "advanced":
                skill_level_id = 3
            elif skill.level == "proficient":
                skill_level_id = 4
            else:
                return f"Unknown proficiency level {skill.level}"

            insert_query_transactional(
                """INSERT INTO cv_skills (skill_id, cv_ad_id, skill_level_id) VALUES (?,?,?)""",
                conn,
                (
                    generated_skill_id,
                    generated_id,
                    skill_level_id,
                ),
            )

        conn.commit()

        cv_ad = CvAd(
            id=generated_id,
            title=cv_ad.title,
            short_desc=cv_ad.short_desc,
            salary_low=cv_ad.salary_low,
            salary_high=cv_ad.salary_high,
            is_remote=cv_ad.is_remote,
            professional_id=professional_id,
            status_id=Status.UNAPPROVED,
            locations_id=location_id,
        )

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    return cv_ad


def all_ads_by_id(professional_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query
    ad_data = get_data_func(
        """
    SELECT ad.id, ad.title, ad.salary_low, ad.salary_high, ad.is_remote, ad.professional_id, t.name, c.name
    FROM cv_ads as ad 
    JOIN locations as l ON ad.locations_id = l.id
    JOIN towns as t ON l.town_id = t.id
    JOIN countries as c ON l.country_id = c.id
    WHERE professional_id = ? and status_id = ?
    """,
        (professional_id, Status.ACTIVE),
    )

    ads = (CVAdResponseModel.from_query_result(*row) for row in ad_data)
    ads_list = []
    for ad in ads:
        ad_skill = get_cv_skills(ad.id)
        ad.skills = ad_skill
        ads_list.append(ad)

    return ads_list


def find_cv_by_id(cv_ad_id: int, get_data_func=None) -> CVAdResponseModel:
    if get_data_func is None:
        get_data_func = read_query

    cv_ad_data = get_data_func(
        """
        SELECT ad.id, ad.title, ad.short_desc, ad.salary_low, ad.salary_high, ad.is_remote, ad.professional_id, t.name, c.name 
        FROM cv_ads as ad
        JOIN locations as l on ad.locations_id = l.id
        JOIN countries as c on l.country_id = c.id
        JOIN towns as t on l.town_id = t.id
        WHERE ad.id = ? and (status_id = ? or status_id = ?)
        """,
        (
            cv_ad_id,
            Status.ACTIVE,
            Status.PRIVATE,
        ),
    )
    if cv_ad_data:
        cv_ad = next(CVAdResponseModel.from_query_result(*row) for row in cv_ad_data)

        ad_skills = get_cv_skills(cv_ad.id)
        cv_ad.skills = ad_skills

        return cv_ad
    return []


def get_cv_skills(cv_ad_id: int) -> list[CreatedSkill]:
    skills_to_return = []
    skills_data = read_query(
        """
            SELECT s.name, l.proficiency_level from cv_ads as cva
            JOIN cv_skills as cvs on cva.id = cvs.cv_ad_id
            JOIN skills as s on cvs.skill_id = s.id
            JOIN levels as l on cvs.skill_level_id = l.id
            WHERE cva.id = ? and s.status_id != 9""",
        (cv_ad_id,),
    )
    ad_skills = (CreatedSkill.from_query_result(*row) for row in skills_data)
    for item in ad_skills:
        skills_to_return.append(item)

    return skills_to_return


def add_skill_to_ad(
    skill: CreatedSkill, cv_ad_id: int, cv_ad: CVAdResponseModel
) -> CVAdResponseModel:
    try:
        conn = _get_connection()
        generated_skill_id = insert_query_transactional(
            """INSERT INTO skills (name, status_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (
                skill.name,
                Status.UNAPPROVED,
            ),  # if added to the table it will be unapproved by default
        )
        if skill.level == "beginner":
            skill_level_id = 1
        elif skill.level == "intermediate":
            skill_level_id = 2
        elif skill.level == "advanced":
            skill_level_id = 3
        elif skill.level == "proficient":
            skill_level_id = 4
        else:
            raise Exception(f'Unknown proficiency level "{skill.level}"')

        insert_query_transactional(
            """INSERT INTO cv_skills (skill_id, cv_ad_id, skill_level_id) VALUES (?,?,?)""",
            conn,
            (
                generated_skill_id,
                cv_ad_id,
                skill_level_id,
            ),
        )
        conn.commit()

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    cv_ad.skills.append(skill)
    return cv_ad


def approve_cv_ad(cv_ad_id: int, new_status: int = Status.APPROVED, update_func=None):
    if update_func is None:
        update_func = update_query

    update_func(
        """UPDATE job_ads SET status_id = ? WHERE id = ?;""", (new_status, cv_ad_id)
    )

    return cv_ad_id


def find_by_criteria(
    title: str | None = None,
    short_desc: str | None = None,
    salary_low: int | None = None,
    salary_high: int | None = None,
    salary_threshold: int | None = None,
    professional_id: int | None = None,
    skills: list[CreatedSkill] | None = None,
    skills_number_threshold: int = 0,
    is_remote: int | None = None,
    town: str | None = None,
    country: str | None = None,
) -> list[CVAdResponseModel]:

    sql_string = """SELECT ad.id, ad.title, ad.short_desc, ad.professional_id, ad.salary_low, ad.salary_high, ad.is_remote, t.name, c.name 
        FROM cv_ads as ad
        JOIN locations as l on ad.locations_id = l.id
        JOIN countries as c on l.country_id = c.id
        JOIN towns as t on l.town_id = t.id
        WHERE status_id = ? """

    added_conditions = []

    if title:
        added_conditions.append(f"ad.title LIKE '%{title}%'")

    if short_desc:
        added_conditions.append(f"ad.description LIKE '%{short_desc}%'")

    if salary_threshold:
        if salary_low:
            salary_low = salary_low * (1 - salary_threshold / 100)
        if salary_high:
            salary_high = salary_high * (1 + salary_threshold / 100)

    if salary_low and salary_high:
        added_conditions.append(
            f"""ad.salary_low BETWEEN {salary_low} AND {salary_high}
         OR ad.salary_high BETWEEN {salary_low} AND {salary_high}
         OR ad.salary_low <= {salary_low} AND ad.salary_high >= {salary_high}"""
        )
    elif salary_low:
        added_conditions.append(
            f"""ad.salary_low <= {salary_low} AND ad.salary_high >= {salary_low}
            OR ad.salary_low > {salary_low}"""
        )
    elif salary_high:
        added_conditions.append(
            f"ad.salary_low <= {salary_high} AND ad.salary_high >= {salary_high}"
        )

    if professional_id:
        added_conditions.append(f"ad.company_id = {professional_id}")

    if is_remote:
        added_conditions.append(f"ad.is_remote = {is_remote}")

    if town:
        if is_remote:
            pass
        else:
            added_conditions.append(f"t.name = '{town}'")

    if country:
        if is_remote:
            pass
        else:
            added_conditions.append(f"c.name = '{country}'")

    join_string = " AND "
    added_sql = join_string.join(added_conditions)
    if added_sql:
        added_sql = "AND (" + added_sql + ")"

    sql_query = sql_string + added_sql

    ad_data = read_query(sql_query, (Status.ACTIVE,))

    found_ads = list(CVAdResponseModel.from_query_result(*row) for row in ad_data)
    ads_to_return = []

    for ad in found_ads:
        response = get_cv_skills_by_criteria(ad.id, skills_number_threshold, skills)
        if response["suitable"]:
            ad.skills = response["skills"]
            ads_to_return.append(ad)

    return ads_to_return


def get_cv_skills_by_criteria(
    cv_ad_id: int,
    skills_number_threshold: int = 0,
    search_skills: list[CreatedSkill] | None = None,
) -> dict[bool, list[CreatedSkill] | None]:

    skills_data = read_query(
        """
        SELECT s.name, l.proficiency_level from job_ads as ja
        JOIN job_skills as js on ja.id = js.job_ad_id
        JOIN skills as s on js.skill_id = s.id
        JOIN levels as l on js.skill_level_id = l.id
        WHERE ja.id = ? and s.status_id = ?""",
        (
            cv_ad_id,
            Status.APPROVED,
        ),
    )
    ad_skills = list(CreatedSkill.from_query_result(*row) for row in skills_data)
    number_of_skills = len(ad_skills)  # Checks how many skills the ad requires
    if search_skills:
        counter = 0
        for skill in search_skills:
            if skill in ad_skills:  # if searched skill is in the ad_skills
                counter += 1
        if counter >= number_of_skills - skills_number_threshold:
            return {"suitable": True, "skills": [ad_skills]}
        else:
            return {"suitable": False, "skills": None}

    return {"suitable": True, "skills": [ad_skills]}
