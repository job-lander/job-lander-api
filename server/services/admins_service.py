from fastapi import Header
from mariadb import IntegrityError
from data.models.admin_models import AdminIn, AdminFromDB
from data.models.company_models import Company, JobAd
from data.models.professional_models import Professional, CvAd
from data.database import insert_query, read_query
from common.responses import NotFound
from common.security import decode_access_token


def create_admin(admin: AdminIn, insert_func=None):
	if insert_func is None:
		insert_func = insert_query
	
	try:
		admin_created_id = insert_func(
			"""INSERT INTO job_lander.admin (username, password, first_name, last_name, email)
	VALUES (?, ?, ?, ?, ?);""",
			(
				admin.username,
				admin.password,
				admin.first_name,
				admin.last_name,
				admin.email,
			),
		)
		return admin_created_id
	except IntegrityError:
		return None


def find_by_username(username: str, read_func=None) -> AdminFromDB | NotFound:
	if read_func is None:
		read_func = read_query
	
	data = read_func(
		"""SELECT *
			FROM job_lander.admin AS a
			WHERE a.username = ?;""",
		(username,),
	)
	
	return next((AdminFromDB.from_db(*row) for row in data), None)


def check_if_admin(token: str = Header(), read_func=None):
	if read_func is None:
		read_func = read_query
	
	payload_token = decode_access_token(token)
	from_token_id = payload_token["sub"]["id"]
	
	result = read_func(
		"""SELECT 1 FROM job_lander.admin AS a WHERE a.id = ?""", (from_token_id,)
	)
	if result:
		return True
	return False


def get_id_from_token(token: str = Header()) -> int:
	payload_token = decode_access_token(token)
	from_token_id = payload_token["sub"]["id"]
	
	return from_token_id


def get_username_from_token(token: str = Header()) -> int:
	payload_token = decode_access_token(token)
	from_token_id = payload_token["sub"]["username"]
	
	return from_token_id


def check_if_owner(
	entity: Company | Professional | CvAd | JobAd, token: str = Header()
) -> bool:
	payload_token = decode_access_token(token)
	from_token_id = payload_token["sub"]["id"]
	
	if isinstance(entity, Company):
		return from_token_id == entity.id
	elif isinstance(entity, Professional):
		return from_token_id == entity.id
	elif isinstance(entity, CvAd):
		return from_token_id == entity.professional_id
	elif isinstance(entity, JobAd):
		return from_token_id == entity.company_id
