from data.database import (
    read_query,
    insert_query_transactional,
    _get_connection,
    update_query,
)
from data.models.common_models import CreatedSkill, CreatedLocation, Status
from data.models.company_models import CreatedJobAd, JobAd, JobAdResponseModel
from services.email_service import notify_admin_for_new_job_ad


def create(
    ad: CreatedJobAd,
    skills: list[CreatedSkill],
    location: CreatedLocation,
    company_id: int,
):

    try:
        conn = _get_connection()

        country_id = insert_query_transactional(
            """INSERT into countries SET name = ? ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (location.country,),
        )

        town_id = insert_query_transactional(
            """INSERT into towns SET name = ? ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (location.town,),
        )

        generated_location_id = insert_query_transactional(
            """INSERT into locations (town_id, country_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (
                town_id,
                country_id,
            ),
        )

        generated_job_ad_id = insert_query_transactional(
            """INSERT INTO job_ads (title, description, salary_low, salary_high, is_remote, company_id, location_id, status_id) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?)""",
            conn,
            (
                ad.title,
                ad.description,
                ad.salary_low,
                ad.salary_high,
                ad.is_remote,
                company_id,
                generated_location_id,
                Status.UNAPPROVED,
            ),
        )

        for skill in skills:
            generated_skill_id = insert_query_transactional(
                """INSERT INTO skills (name, status_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
                conn,
                (
                    skill.name,
                    Status.UNAPPROVED,
                ),  # if added to the table it will be unapproved
            )
            if skill.level == "beginner":
                skill_level_id = 1
            elif skill.level == "intermediate":
                skill_level_id = 2
            elif skill.level == "advanced":
                skill_level_id = 3
            elif skill.level == "proficient":
                skill_level_id = 4
            else:
                return f"Unknown proficiency level {skill.level}"

            insert_query_transactional(
                """INSERT INTO job_skills (skill_id, job_ad_id, skill_level_id) VALUES (?,?,?)""",
                conn,
                (
                    generated_skill_id,
                    generated_job_ad_id,
                    skill_level_id,
                ),
            )

        conn.commit()

        created_ad = JobAd(
            id=generated_job_ad_id,
            title=ad.title,
            description=ad.description,
            salary_low=ad.salary_low,
            salary_high=ad.salary_high,
            is_remote=ad.is_remote,
            company_id=company_id,
            location_id=generated_location_id,
            status_id=Status.UNAPPROVED,
        )

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    notify_admin_for_new_job_ad(created_ad.id)  # notifies admin by email

    return created_ad


def all(company_id: int, job_status: int) -> list[JobAdResponseModel]:
    ad_data = read_query(
        """
        SELECT ad.id, ad.title, ad.description, ad.company_id, ad.salary_low, ad.salary_high, ad.is_remote, ad.status_id, t.name, c.name 
        FROM job_ads as ad
        JOIN locations as l on ad.location_id = l.id
        JOIN countries as c on l.country_id = c.id
        JOIN towns as t on l.town_id = t.id
        WHERE company_id = ? and status_id = ?
        """,
        (
            company_id,
            job_status,
        ),
    )
    ads = (JobAdResponseModel.from_query_result(*row) for row in ad_data)
    ads_to_return = []
    for ad in ads:
        ad_skills = get_job_skills(ad.id)
        ad.skills = ad_skills
        ads_to_return.append(ad)

    return ads_to_return


def find_by_id(job_ad_id: int) -> JobAdResponseModel:
    ad_data = read_query(
        """
        SELECT ad.id, ad.title, ad.description, ad.company_id, ad.salary_low, ad.salary_high, ad.is_remote, ad.status_id, t.name, c.name 
        FROM job_ads as ad
        JOIN locations as l on ad.location_id = l.id
        JOIN countries as c on l.country_id = c.id
        JOIN towns as t on l.town_id = t.id
        WHERE ad.id = ?
        """,
        (job_ad_id,),
    )
    if ad_data:
        ad = next(JobAdResponseModel.from_query_result(*row) for row in ad_data)

        ad_skills = get_job_skills(ad.id)
        ad.skills = ad_skills

        return ad

    return []


def get_job_skills(job_ad_id: int) -> list[CreatedSkill]:
    skills_to_return = []
    skills_data = read_query(
        """
        SELECT s.name, l.proficiency_level from job_ads as ja
        JOIN job_skills as js on ja.id = js.job_ad_id
        JOIN skills as s on js.skill_id = s.id
        JOIN levels as l on js.skill_level_id = l.id
        WHERE ja.id = ? and s.status_id = ?""",
        (
            job_ad_id,
            Status.APPROVED,
        ),
    )
    ad_skills = (CreatedSkill.from_query_result(*row) for row in skills_data)
    for item in ad_skills:
        skills_to_return.append(item)

    return skills_to_return


def add_skill_to_ad(
    skill: CreatedSkill, job_ad_id: int, job_ad: JobAdResponseModel
) -> JobAdResponseModel:
    try:
        conn = _get_connection()
        generated_skill_id = insert_query_transactional(
            """INSERT INTO skills (name, status_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (
                skill.name,
                Status.UNAPPROVED,
            ),  # if added to the table it will be unapproved by default
        )
        if skill.level == "beginner":
            skill_level_id = 1
        elif skill.level == "intermediate":
            skill_level_id = 2
        elif skill.level == "advanced":
            skill_level_id = 3
        elif skill.level == "proficient":
            skill_level_id = 4
        else:
            raise Exception(f'Unknown proficiency level "{skill.level}"')

        insert_query_transactional(
            """INSERT INTO job_skills (skill_id, job_ad_id, skill_level_id) VALUES (?,?,?)""",
            conn,
            (
                generated_skill_id,
                job_ad_id,
                skill_level_id,
            ),
        )
        conn.commit()

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    job_ad.skills.append(skill)
    return job_ad


def find_by_criteria(
    title: str | None = None,
    description: str | None = None,
    salary_low: int | None = None,
    salary_high: int | None = None,
    salary_treshold: int | None = None,
    company_id: int | None = None,
    skills: list[CreatedSkill] | None = None,
    skills_number_treshold: int = 0,
    is_remote: int | None = None,
    status_id: int | None = Status.ACTIVE,
    town: str | None = None,
    country: str | None = None,
) -> list[JobAdResponseModel]:

    sql_string = """SELECT ad.id, ad.title, ad.description, ad.company_id, ad.salary_low, ad.salary_high, ad.is_remote, ad.status_id, t.name, c.name 
        FROM job_ads as ad
        JOIN locations as l on ad.location_id = l.id
        JOIN countries as c on l.country_id = c.id
        JOIN towns as t on l.town_id = t.id
        WHERE status_id = ? """

    added_conditions = []

    if title:
        added_conditions.append(f"ad.title LIKE '%{title}%'")

    if description:
        added_conditions.append(f"ad.description LIKE '%{description}%'")

    if salary_treshold:
        if salary_low:
            salary_low = salary_low * (1 - salary_treshold / 100)
        if salary_high:
            salary_high = salary_high * (1 + salary_treshold / 100)

    if salary_low and salary_high:
        added_conditions.append(
            f"""ad.salary_low BETWEEN {salary_low} AND {salary_high}
         OR ad.salary_high BETWEEN {salary_low} AND {salary_high}
         OR ad.salary_low <= {salary_low} AND ad.salary_high >= {salary_high}"""
        )
    elif salary_low:
        added_conditions.append(
            f"""ad.salary_low <= {salary_low} AND ad.salary_high >= {salary_low}
            OR ad.salary_low > {salary_low}"""
        )
    elif salary_high:
        added_conditions.append(
            f"ad.salary_low <= {salary_high} AND ad.salary_high >= {salary_high}"
        )

    if company_id:
        added_conditions.append(f"ad.company_id = {company_id}")

    if is_remote:
        added_conditions.append(f"ad.is_remote = {is_remote}")

    if status_id:
        added_conditions.append(f"ad.status_id = {status_id}")

    if town:
        if is_remote:
            pass
        else:
            added_conditions.append(f"t.name = '{town}'")

    if country:
        if is_remote:
            pass
        else:
            added_conditions.append(f"c.name = '{country}'")

    join_string = " AND "
    added_sql = join_string.join(added_conditions)
    if added_sql:
        added_sql = "AND (" + added_sql + ")"

    sql_query = sql_string + added_sql

    ad_data = read_query(sql_query, (status_id,))

    found_ads = list(JobAdResponseModel.from_query_result(*row) for row in ad_data)
    ads_to_return = []

    for ad in found_ads:
        response = get_job_skills_by_criteria(ad.id, skills_number_treshold, skills)
        if response["suitable"]:
            ad.skills = response["skills"]
            ads_to_return.append(ad)

    return ads_to_return


def get_job_skills_by_criteria(
    job_ad_id: int,
    skills_number_treshold: int = 0,
    search_skills: list[CreatedSkill] | None = None,
) -> dict[bool, list[CreatedSkill] | None]:

    skills_data = read_query(
        """
        SELECT s.name, l.proficiency_level from job_ads as ja
        JOIN job_skills as js on ja.id = js.job_ad_id
        JOIN skills as s on js.skill_id = s.id
        JOIN levels as l on js.skill_level_id = l.id
        WHERE ja.id = ? and s.status_id = ?""",
        (
            job_ad_id,
            Status.APPROVED,
        ),
    )
    ad_skills = list(CreatedSkill.from_query_result(*row) for row in skills_data)
    number_of_skills = len(ad_skills)  # Checks how many skills the ad requires
    if search_skills:
        counter = 0
        for skill in search_skills:
            if skill in ad_skills:  # if searched skill is in the ad_skills
                counter += 1
        if counter >= number_of_skills - skills_number_treshold:
            return {"suitable": True, "skills": [ad_skills]}
        else:
            return {"suitable": False, "skills": None}

    return {"suitable": True, "skills": [ad_skills]}


def approve_job_ad(job_ad_id: int, new_status: int = Status.APPROVED, update_func=None):
    if update_func is None:
        update_func = update_query

    update_func(
        """UPDATE job_ads SET status_id = ? WHERE id = ?;""", (new_status, job_ad_id)
    )
    return job_ad_id


def activate_job_ad(job_ad_id: int, new_status: int = Status.ACTIVE, update_func=None):
    if update_func is None:
        update_func = update_query

    update_func(
        """UPDATE job_ads SET status_id = ? WHERE id = ?;""", (new_status, job_ad_id)
    )
    return job_ad_id
