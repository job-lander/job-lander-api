from data.database import (
    _get_connection,
    insert_query,
    read_query,
    update_query_transactional,
)
from data.models.common_models import Status
from services.email_service import (
    notify_company_for_new_match_request,
    notify_professional_for_new_match_request,
)
from data.models.match_request_models import (
    MRCompanyReceived_toJobAd,
    MRCompanySent_toCVad,
    MRCompanySentOrReceived_JobAdtoCVad,
    MRequestProfessionalReceived_toCVAd,
    MRequestProfessionalSent_toJobAd,
    MRProfessionalSentOrReceived_CVAdtoJobad,
)


def match_request_professional_to_jobad(
    professional_id: int,
    job_ad_id: int,
    professional_username: str,
    insert_query_func=None,
):
    if insert_query_func == None:
        insert_query_func = insert_query

    match_request = insert_query_func(
        """INSERT INTO match_request_professional_to_ad (professional_id, job_ad_id) VALUES (?,?)""",
        (professional_id, job_ad_id),
    )
    notify_company_for_new_match_request(professional_username)
    return match_request


def match_request_company_to_cvad(
    company_id: int, cv_ad_id: int, company_name: str, insert_query_func=None
):
    if insert_query_func == None:
        insert_query_func = insert_query

    match_request = insert_query_func(
        """INSERT INTO match_request_company_to_cv (company_id, cv_ad_id) VALUES (?,?)""",
        (company_id, cv_ad_id),
    )
    notify_professional_for_new_match_request(company_name)
    return match_request


def match_request_professional_cvad_to_jobad(
    cv_ad_id: int, job_ad_id: int, professional_name: str, insert_query_func=None
) -> bool:
    if insert_query_func == None:
        insert_query_func = insert_query

    match_request = insert_query_func(
        """INSERT INTO match_request_cv_to_ad (job_ad_id, cv_ad_id, sent_by_company) VALUES (?,?,?)""",
        (job_ad_id, cv_ad_id, 0),
    )
    notify_company_for_new_match_request(professional_name)
    return match_request


def match_request_company_jobad_to_cvad(
    job_ad_id: int, cv_ad_id: int, company_name: str, insert_query_func=None
) -> bool:
    if insert_query_func == None:
        insert_query_func = insert_query

    match_request = insert_query_func(
        """INSERT INTO match_request_cv_to_ad (job_ad_id, cv_ad_id, sent_by_company) VALUES (?,?,?)""",
        (job_ad_id, cv_ad_id, 1),
    )
    notify_professional_for_new_match_request(company_name)
    return match_request


def view_match_requests_sent_to_company(company_id, job_ad_id, read_query_func=None):
    if read_query_func == None:
        read_query_func = read_query

    sql_query_cv_to_ad = """
    SELECT mr.id, ja.company_id, c.name as company_name, mr.job_ad_id, mr.cv_ad_id, p.username as professional_username
    FROM match_request_cv_to_ad as mr
    JOIN job_ads as ja on mr.job_ad_id=ja.id
    JOIN companies as c on ja.company_id=c.id
    JOIN cv_ads as ca on mr.cv_ad_id=ca.id
    JOIN professionals as p on ca.professional_id=p.id
    WHERE ja.company_id = ?
    AND mr.sent_by_company = 0"""

    if job_ad_id:
        sql_query_cv_to_ad += "AND mr.job_ad_id = {job_ad_id}"

    match_requests_cv_to_ad = read_query_func(sql_query_cv_to_ad, (company_id,))

    sql_query_professional_to_ad = """
    SELECT mr.id, ja.company_id, c.name as company_name, mr.job_ad_id, p.username as professional_username 
    FROM match_request_professional_to_ad as mr
    JOIN job_ads as ja on mr.job_ad_id=ja.id
    JOIN companies as c on ja.company_id=c.id
    JOIN professionals as p on mr.professional_id=p.id
    WHERE ja.company_id = ?"""

    if job_ad_id:
        sql_query_cv_to_ad += "AND mr.job_ad_id = {job_ad_id}"

    match_requests_professional_to_ad = read_query_func(
        sql_query_professional_to_ad, (company_id,)
    )

    return {
        "Match requests company recaived cv to ad": (
            MRCompanySentOrReceived_JobAdtoCVad.from_query_result(*row)
            for row in match_requests_cv_to_ad
        ),
        "Match requests company received to ad": (
            MRCompanyReceived_toJobAd.from_query_result(*row)
            for row in match_requests_professional_to_ad
        ),
    }


def view_match_requests_sent_by_company(company_id, job_ad_id, read_query_func=None):
    if read_query_func == None:
        read_query_func = read_query

    sql_query_ad_to_cv = """
    SELECT mr.id, ja.company_id, c.name as company_name, mr.job_ad_id, mr.cv_ad_id, p.username as professional_username
    FROM match_request_cv_to_ad as mr
    JOIN job_ads as ja on mr.job_ad_id=ja.id
    JOIN companies as c on ja.company_id=c.id
    JOIN cv_ads as ca on mr.cv_ad_id=ca.id
    JOIN professionals as p on ca.professional_id=p.id
    WHERE ja.company_id = ?
    AND mr.sent_by_company = 1"""

    if job_ad_id:
        sql_query_ad_to_cv += "AND mr.job_ad_id = {job_ad_id}"

    match_requests_ad_to_cv = read_query_func(sql_query_ad_to_cv, (company_id,))

    sql_query_company_to_cv = """
    SELECT mr.id, mr.company_id as company_id, c.name as company_name, mr.cv_ad_id, p.username as professional_username
    FROM match_request_company_to_cv as mr
    JOIN cv_ads as ca on mr.cv_ad_id=ca.id
    JOIN professionals as p on ca.professional_id=p.id
    JOIN companies as c on mr.company_id=c.id
    WHERE mr.company_id = ?"""

    match_requests_company_to_cv = read_query_func(
        sql_query_company_to_cv, (company_id,)
    )

    return {
        "Match requests company sent cv to ad": (
            MRCompanySentOrReceived_JobAdtoCVad.from_query_result(*row)
            for row in match_requests_ad_to_cv
        ),
        "Match requests company sent to ad": (
            MRCompanySent_toCVad.from_query_result(*row)
            for row in match_requests_company_to_cv
        ),
    }


def view_match_requests_sent_to_professional(
    professional_id, cv_ad_id, read_query_func=None
):
    if read_query_func == None:
        read_query_func = read_query

    sql_query_ad_to_cv = """
    SELECT mr.id, ca.professional_id as professional_id, p.username as professional_username, mr.cv_ad_id, mr.job_ad_id, c.name as company_name
    FROM match_request_cv_to_ad as mr
    JOIN job_ads as ja on mr.job_ad_id=ja.id
    JOIN companies as c on ja.company_id=c.id
    JOIN cv_ads as ca on mr.cv_ad_id=ca.id
    JOIN professionals as p on ca.professional_id=p.id
    WHERE ca.professional_id = ?
    AND mr.sent_by_company = 1"""

    if cv_ad_id:
        sql_query_ad_to_cv += "AND mr.cv_ad_id = {cv_ad_id}"

    match_requests_jobad_to_cv = read_query_func(sql_query_ad_to_cv, (professional_id,))

    sql_query_company_to_cv = """
    SELECT mr.id, p.id as professional_id, p.username as professional_username, mr.cv_ad_id, c.name as company_name
    FROM match_request_company_to_cv as mr
    JOIN cv_ads as ca on mr.cv_ad_id=ca.id
    JOIN professionals as p on ca.professional_id=p.id
    JOIN companies as c on mr.company_id=c.id
    WHERE ca.professional_id = ?"""

    if cv_ad_id:
        sql_query_company_to_cv += "AND mr.cv_ad_id = {cv_ad_id}"

    match_requests_company_to_cv = read_query_func(
        sql_query_company_to_cv, (professional_id,)
    )

    return {
        "Match requests professional received ad to cv": (
            MRProfessionalSentOrReceived_CVAdtoJobad.from_query_result(*row)
            for row in match_requests_jobad_to_cv
        ),
        "Match requests professional received to cv": (
            MRequestProfessionalReceived_toCVAd.from_query_result(*row)
            for row in match_requests_company_to_cv
        ),
    }


def view_match_requests_sent_by_professional(
    professional_id,
    cv_ad_id,
    read_query_func_cv_to_ad=None,
    read_query_func_professional_to_ad=None,
):
    if read_query_func_cv_to_ad == None:
        read_query_func_cv_to_ad = read_query

    sql_query_cv_to_ad = """
    SELECT mr.id, ca.professional_id as professional_id, p.username as professional_username, mr.cv_ad_id, mr.job_ad_id, c.name as company_name
    FROM match_request_cv_to_ad as mr
    JOIN job_ads as ja on mr.job_ad_id=ja.id
    JOIN companies as c on ja.company_id=c.id
    JOIN cv_ads as ca on mr.cv_ad_id=ca.id
    JOIN professionals as p on ca.professional_id=p.id
    WHERE ca.professional_id = ?
    AND mr.sent_by_company = 0"""

    if cv_ad_id:
        sql_query_cv_to_ad += "AND mr.cv_ad_id = {cv_ad_id}"

    match_requests_jobad_to_cv = read_query_func_cv_to_ad(
        sql_query_cv_to_ad, (professional_id,)
    )

    if read_query_func_professional_to_ad == None:
        read_query_func_professional_to_ad = read_query

    sql_query_professional_to_ad = """
    SELECT mr.id, mr.professional_id as professional_id, p.username as professional_username, mr.job_ad_id, c.name as company_name
    FROM match_request_professional_to_ad as mr
    JOIN job_ads as ja on mr.job_ad_id=ja.id
    JOIN companies as c on ja.company_id=c.id
    JOIN professionals as p on mr.professional_id=p.id
    WHERE mr.professional_id = ?"""

    match_requests_company_to_cv = read_query_func_professional_to_ad(
        sql_query_professional_to_ad, (professional_id,)
    )

    return {
        "Match requests professional sent cv to ad": (
            MRProfessionalSentOrReceived_CVAdtoJobad.from_query_result(*row)
            for row in match_requests_jobad_to_cv
        ),
        "Match requests professional sent to ad": (
            MRequestProfessionalSent_toJobAd.from_query_result(*row)
            for row in match_requests_company_to_cv
        ),
    }


# update match_request_professional_to_ad
# set job_ad - archived, optionally - leave ad active
# set professional - busy
def company_confirm_match_request_professional_to_ad(
    company_id: int,
    match_request_id: int,
    archivate_job_ad: bool | None = None,
    read_match_query=None,
    update_professional_query_func=None,
    update_match_request_query_func=None,
    update_job_ad_query_func=None,
):
    if read_match_query == None:
        read_match_query = read_query

    # check if this match request id is made to this company
    match_data = read_match_query(
        """
        SELECT mr.professional_id, p.username, mr.job_ad_id, mr.id, ja.company_id 
        FROM match_request_professional_to_ad as mr
        JOIN job_ads as ja on mr.job_ad_id=ja.id
        JOIN professionals as p on mr.professional_id=p.id
        WHERE ja.company_id = ? AND mr.id = ?""",
        (company_id, match_request_id),
    )
    if not match_data:
        return f"Invalid match request"

    professional_id = match_data[0][0]
    professional_username = match_data[0][1]
    job_ad_id = match_data[0][2]

    try:
        conn = _get_connection()

        if update_professional_query_func == None:
            update_professional_query_func = update_query_transactional

        update_professional_query_func(
            """
        UPDATE professionals SET status_id = ? WHERE id = ?""",
            conn,
            (Status.BUSY, professional_id),
        )

        if update_match_request_query_func == None:
            update_match_request_query_func = update_query_transactional

        update_match_request_query_func(
            """
            UPDATE match_request_professional_to_ad SET confirmed=1 where id = ?""",
            conn,
            (match_request_id,),
        )

        if archivate_job_ad:
            if update_job_ad_query_func == None:
                update_job_ad_query_func = update_query_transactional

            update_job_ad_query_func(
                """
            UPDATE job_ads
            SET status_id = ?
            WHERE id = ?""",
                conn,
                (Status.ARCHIVED, job_ad_id),
            )

        conn.commit()

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    return {"professional": professional_username}


# update match_request_cv_to_ad
# set job_ad - archived, optionally - leave ad active
# set professional - busy
def company_confirm_match_request_cv_to_ad(
    company_id: int,
    match_request_id: int,
    archivate_job_ad: bool | None = None,
    read_match_query=None,
    update_professional_query_func=None,
    update_match_request_query_func=None,
    update_job_ad_query_func=None,
):
    if read_match_query == None:
        read_match_query = read_query

    # check if this match request id is made to this company
    match_data = read_match_query(
        """
    SELECT p.id, p.username, mr.job_ad_id, mr.cv_ad_id, mr.id, ja.company_id 
    FROM match_request_cv_to_ad as mr
    JOIN job_ads as ja on mr.job_ad_id=ja.id
    JOIN cv_ads as ca on mr.cv_ad_id=ca.id
    JOIN professionals as p on ca.professional_id=p.id
    WHERE ja.company_id = ? AND mr.id = ? AND sent_by_company = 0""",
        (company_id, match_request_id),
    )
    if not match_data:
        return f"Invalid match request"

    professional_id = match_data[0][0]
    professional_username = match_data[0][1]
    job_ad_id = match_data[0][2]
    cv_ad_id = match_data[0][3]

    try:
        conn = _get_connection()

        if update_professional_query_func == None:
            update_professional_query_func = update_query_transactional

        update_professional_query_func(
            """
        UPDATE professionals SET status_id = ? WHERE id = ?""",
            conn,
            (Status.BUSY, professional_id),
        )

        if update_match_request_query_func == None:
            update_match_request_query_func = update_query_transactional

        update_match_request_query_func(
            """
            UPDATE match_request_cv_to_ad
            SET confirmed = 1
            WHERE id = ?""",
            conn,
            (match_request_id),
        )

        if archivate_job_ad:
            if update_job_ad_query_func == None:
                update_job_ad_query_func = update_query_transactional

            update_job_ad_query_func(
                """
            UPDATE job_ads
            SET status_id = ?
            WHERE id = ?""",
                conn,
                (Status.ARCHIVED, job_ad_id),
            )

        conn.commit()

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    return {"professional": professional_username, "cv_ad_id": cv_ad_id}


def professional_confirm_match_request_company_to_cv(
    professional_id: int,
    match_request_id: int,
    read_match_query=None,
    update_professional_query_func=None,
    update_match_request_query_func=None,
    update_cv_ad_query_func=None,
):
    if read_match_query == None:
        read_match_query = read_query

    # check if this match request id is made to this professional
    match_data = read_match_query(
        """
        SELECT mr.company_id, c.name, mr.cv_ad_id, mr.id, ca.professional_id
        FROM match_request_company_to_cv as mr
        JOIN cv_ads as ca on mr.cv_ad_id=ca.id
        JOIN companies as c on mr.company_id=c.id
        WHERE ca.professional_id = ? AND mr.id = ?""",
        (professional_id, match_request_id),
    )
    if not match_data:
        return f"Invalid match request"

    company_name = match_data[0][1]
    cv_ad_id = match_data[0][2]

    try:
        conn = _get_connection()

        if update_professional_query_func == None:
            update_professional_query_func = update_query_transactional

        update_professional_query_func(
            """
        UPDATE professionals SET status_id = ? WHERE id = ?""",
            conn,
            (Status.BUSY, professional_id),
        )

        if update_match_request_query_func == None:
            update_match_request_query_func = update_query_transactional

        update_match_request_query_func(
            """
            UPDATE match_request_company_to_cv
            SET confirmed = 1
            WHERE id = ?""",
            conn,
            (match_request_id),
        )

        if update_cv_ad_query_func == None:
            update_cv_ad_query_func = update_query_transactional

            update_cv_ad_query_func(
                """
            UPDATE cv_ads
            SET status_id = ?
            WHERE id = ?""",
                conn,
                (Status.MATCHED, cv_ad_id),
            )

        conn.commit()

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    return {"company": company_name}


def professional_confirm_match_request_ad_to_cv(
    professional_id: int,
    match_request_id: int,
    read_match_query=None,
    update_professional_query_func=None,
    update_match_request_query_func=None,
    update_cv_ad_query_func=None,
):
    if read_match_query == None:
        read_match_query = read_query

    # check if this match request id is made to this company
    match_data = read_match_query(
        """
    SELECT c.id, c.name, mr.job_ad_id, mr.cv_ad_id, mr.id, ja.company_id 
    FROM match_request_cv_to_ad as mr
    JOIN job_ads as ja on mr.job_ad_id=ja.id
    JOIN cv_ads as ca on mr.cv_ad_id=ca.id
    JOIN professionals as p on ca.professional_id=p.id
    JOIN companies as c on ja.company_id=c.id
    WHERE ca.professional_id = ? AND mr.id = ? AND sent_by_company = 1""",
        (professional_id, match_request_id),
    )
    if not match_data:
        return f"Invalid match request"

    company_name = match_data[0][1]
    job_ad_id = match_data[0][2]
    cv_ad_id = match_data[0][3]

    try:
        conn = _get_connection()

        if update_professional_query_func == None:
            update_professional_query_func = update_query_transactional

        update_professional_query_func(
            """
        UPDATE professionals SET status_id = ? WHERE id = ?""",
            conn,
            (Status.BUSY, professional_id),
        )

        if update_match_request_query_func == None:
            update_match_request_query_func = update_query_transactional

        update_match_request_query_func(
            """
            UPDATE match_request_cv_to_ad
            SET confirmed = 1
            WHERE id = ?""",
            conn,
            (match_request_id),
        )

        if update_cv_ad_query_func == None:
            update_cv_ad_query_func = update_query_transactional

            update_cv_ad_query_func(
                """
            UPDATE job_ads
            SET status_id = ?
            WHERE id = ?""",
                conn,
                (Status.MATCHED, cv_ad_id),
            )

        conn.commit()

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    return {"company": company_name, "job_ad_id": job_ad_id}
