from data.database import insert_query, read_query


def upload(file: bytes, insert_data_func=None):
    if insert_data_func == None:
        insert_data_func = insert_query
    generated_id = (
        insert_data_func("""INSERT INTO images (image) VALUES (?)""", (file,)),
    )

    return generated_id


def see_image(id: int):
    image = read_query("""SELECT image FROM images WHERE id = ? """, (id,))

    return image
