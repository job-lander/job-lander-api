from data.database import (
    read_query,
    read_query_to_dict,
    insert_query,
    insert_query_transactional,
    update_query,
    _get_connection,
)
from data.models.company_models import (
    CreatedCompany,
    CreatedContacts,
    Company,
    CompanyContacts,
    Contacts,
)
from data.models.common_models import Status

from common.security import get_password_hash
from data.models.common_models import CreatedLocation


def create(
    company: CreatedCompany,
    contacts: CreatedContacts,
    location: CreatedLocation,
    insert_company_data_func=None,
    insert_contacts_data_func=None,
    get_country_id_func=None,
    insert_town_func=None,
    get_location_id_func=None,
):

    try:
        conn = _get_connection()

        if get_country_id_func == None:
            get_country_id_func = read_query
        country_id = get_country_id_func(
            """SELECT id from countries WHERE name = ?""", conn, (location.country,)
        )
        if not country_id:
            raise Exception("No such country")

        if insert_town_func == None:
            insert_town_func = insert_query_transactional
        town_id = insert_town_func(
            """INSERT into towns SET name = ? ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (location.town,),
        )

        if get_location_id_func == None:
            get_location_id_func = insert_query_transactional
        location_id = get_location_id_func(
            """INSERT into locations (town_id, country_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (
                town_id,
                country_id[0][0],
            ),
        )

        encrypted_password = get_password_hash(company.password)

        if insert_company_data_func is None:
            insert_company_data_func = insert_query_transactional
        generated_company_id = insert_company_data_func(
            """INSERT INTO companies (username, name, password, description, UIC, website, twitter, logo_id, status_id)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)""",
            conn,
            (
                company.username,
                company.name,
                encrypted_password,
                company.description,
                company.UIC,
                company.website,
                company.twitter,
                1,  # default logo
                Status.UNAPPROVED,
            ),
        )

        if insert_contacts_data_func == None:
            insert_contacts_data_func = insert_query_transactional
        generated_contacts_id = insert_contacts_data_func(
            """INSERT INTO contacts (address, phone,
        email, location_id, company_id) VALUES (?, ?, ?, ?, ?)""",
            conn,
            (
                contacts.address,
                contacts.phone,
                contacts.email,
                location_id,
                generated_company_id,
            ),
        )

        conn.commit()

        created_company = Company(
            id=generated_company_id,
            username=company.username,
            name=company.name,
            password=company.password,
            description=company.description,
            UIC=company.UIC,
            website=company.website,
            twitter=company.twitter,
            logo_id=1,
            status_id=Status.UNAPPROVED,
        )

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    return created_company


def get_company_info(company_id: int, get_company_func=None) -> dict | None:
    if get_company_func == None:
        get_company_func = read_query_to_dict
    company_info = get_company_func(
        """
        SELECT comp.name, comp.description, comp.UIC, comp.website, comp.twitter, i.image from companies as comp
        JOIN images as i on comp.logo_id = i.id
        WHERE comp.id = ?""",
        (company_id,),
    )
    return company_info


# add company office
def add_contact(
    company_id,
    contacts: CreatedContacts,
    location: CreatedLocation,
    get_country_id_func=None,
    insert_town_func=None,
    get_location_id_func=None,
    insert_contacts_data_func=None,
):

    try:
        conn = _get_connection()

        if get_country_id_func == None:
            get_country_id_func = read_query
        country_id = get_country_id_func(
            """SELECT id from countries WHERE name = ?""", conn, (location.country,)
        )
        if not country_id:
            raise Exception("No such country")

        if insert_town_func == None:
            insert_town_func = insert_query_transactional
        town_id = insert_town_func(
            """INSERT into towns SET name = ? ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (location.town,),
        )

        if get_location_id_func == None:
            get_location_id_func = insert_query_transactional
        location_id = get_location_id_func(
            """INSERT into locations (town_id, country_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (
                town_id,
                country_id[0][0],
            ),
        )

        if insert_contacts_data_func == None:
            insert_contacts_data_func = insert_query_transactional
        generated_contacts_id = insert_contacts_data_func(
            """INSERT INTO contacts (address, phone,
        email, location_id, company_id) VALUES (?, ?, ?, ?, ?)""",
            conn,
            (
                contacts.address,
                contacts.phone,
                contacts.email,
                location_id,
                company_id,
            ),
        )
        conn.commit()

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    return CompanyContacts(
        id=generated_contacts_id,
        town=location.town,
        country=location.country,
        address=contacts.address,
        phone=contacts.phone,
        email=contacts.email,
    )


def edit_contact(
    contact_id: int,
    new_contact: CreatedContacts,
    new_location: CreatedLocation,
    get_old_contact_func=None,
    get_country_id_func=None,
    insert_town_func=None,
    get_location_id_func=None,
    update_contacts_data_func=None,
) -> CompanyContacts:

    if get_old_contact_func == None:
        get_old_contact_func = read_query_to_dict
    old_contact = get_old_contact_func(
        """
    SELECT
    cont.company_id, cont.address, cont.phone, cont.email, t.name as town_name, c.name as country_name, t.id as town_id, c.id as country_id
    FROM contacts as cont
    JOIN locations as l on cont.location_id = l.id
    JOIN towns as t on l.town_id = t.id
    JOIN countries as c on l.country_id = c.id
    WHERE cont.id = ?""",
        (contact_id,),
    )

    if new_location.country and new_location.country != old_contact["country_name"]:
        if get_country_id_func == None:
            get_country_id_func = read_query
        country_id = get_country_id_func(
            """SELECT id from countries WHERE name = ?""", (new_location.country,)
        )[0][0]
    else:
        country_id = old_contact["country_id"]

    try:
        conn = _get_connection()
        if new_location.town and new_location.town != old_contact["town_name"]:
            if insert_town_func == None:
                insert_town_func = insert_query_transactional
            town_id = insert_town_func(
                """INSERT into towns SET name = ? ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
                conn,
                (new_location.town,),
            )
        else:
            town_id = old_contact["town_id"]

        if get_location_id_func == None:
            get_location_id_func = insert_query_transactional
        location_id = get_location_id_func(
            """INSERT into locations (town_id, country_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (
                town_id,
                country_id,
            ),
        )

        conn.commit()

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()

    merged_contact = Contacts(
        id=contact_id,
        address=new_contact.address or old_contact["address"],
        phone=new_contact.phone or old_contact["phone"],
        email=new_contact.email or old_contact["email"],
        location_id=location_id,
        company_id=old_contact["company_id"],
    )

    if update_contacts_data_func == None:
        update_contacts_data_func = update_query
    updated_contact = update_contacts_data_func(
        """UPDATE contacts
        SET address = ?, phone = ?, email = ?, location_id = ?
        WHERE id = ?""",
        (
            merged_contact.address,
            merged_contact.phone,
            merged_contact.email,
            location_id,
            contact_id,
        ),
    )
    return CompanyContacts(
        id=contact_id,
        town=new_location.town,
        country=new_location.country,
        address=merged_contact.address,
        phone=merged_contact.phone,
        email=merged_contact.email,
    )


def get_contacts(company_id: int, get_contacts_func=None) -> list[CompanyContacts]:
    if get_contacts_func == None:
        get_contacts_func = read_query
    contacts_data = get_contacts_func(
        """
        SELECT cont.id, t.name, c.name, cont.address, cont.phone, cont.email from contacts as cont
        JOIN locations as l ON cont.location_id = l.id
        JOIN towns as t on l.town_id = t.id
        JOIN countries as c on l.country_id = c.id
        WHERE cont.company_id = ?""",
        (company_id,),
    )
    if contacts_data:
        return (CompanyContacts.from_query_result(*row) for row in contacts_data)
    return contacts_data


def get_contact_by_id(
    company_id: int, contact_id: int, get_contacts_func=None
) -> Contacts | None:
    if get_contacts_func == None:
        get_contacts_func = read_query
    contacts_data = get_contacts_func(
        """
        SELECT id, address, phone, email, location_id, company_id
        FROM contacts
        WHERE company_id = ? and id = ?""",
        (company_id, contact_id),
    )
    return next((Contacts.from_query_result(*row) for row in contacts_data), None)


def get_company_job_ads_stats(company_id: int, get_data_func=None) -> dict:
    if get_data_func == None:
        get_data_func = read_query

    # 1 connection/query method
    job_ads = get_data_func(
        """SELECT status_id from job_ads where company_id = ?""",
        (company_id,),
    )

    number_of_active_job_ads = 0
    number_of_matched_job_ads = 0
    if job_ads:
        for ad in job_ads:
            if ad[0] == Status.ACTIVE:
                number_of_active_job_ads += 1
            elif ad[0] == Status.MATCHED:
                number_of_matched_job_ads += 1

    # 2 connections method
    # number_of_active_job_ads = read_query(
    #     """SELECT count(id) from job_ads where company_id = ? and status_id = ?""",
    #     (
    #         company_id,
    #         1, # status = 1 - active
    #     ),
    # )
    # number_of_matched_job_ads = read_query(
    #     """SELECT count(id) from job_ads where company_id = ? and status_id = ?""",
    #     (
    #         company_id,
    #         5, # status = 5 - matched
    #     ),
    # )
    return {
        "active_ads": number_of_active_job_ads,
        "matched_ads": number_of_matched_job_ads,
    }


def upload_logo(
    file: bytes, company_id: int, insert_data_func=None, update_logo_func=None
) -> bool:
    if insert_data_func == None:
        insert_data_func = insert_query
    generated_id = insert_data_func(
        """INSERT INTO images (image) VALUES (?)""", (file,)
    )

    if update_logo_func == None:
        update_logo_func = update_query
    updated_logo = update_logo_func(
        """UPDATE companies SET logo_id = ? WHERE id = ?""",
        (
            generated_id,
            company_id,
        ),
    )
    return updated_logo


def edit_company_info(
    company: Company, data: CreatedCompany, update_info_func=None
) -> CreatedCompany:
    encrypted_password = get_password_hash(data.password)

    merged = CreatedCompany(
        username=data.username or company.username,
        name=data.name or company.name,
        password=encrypted_password or company.password,
        description=data.description or company.description,
        UIC=data.UIC or company.UIC,
        website=data.website or company.website,
        twitter=data.twitter or company.twitter,
    )
    if update_info_func == None:
        update_info_func = update_query

    update_info_func(
        """
    UPDATE companies
    SET username = ?, name = ?, password = ?, description = ?, UIC = ?, website = ?, twitter = ?
    WHERE id = ?""",
        (
            merged.username,
            merged.name,
            merged.password,
            merged.description,
            merged.UIC,
            merged.website,
            merged.twitter,
            company.id,
        ),
    )
    return merged


def find_by_username(username: str, get_data_func=None) -> Company | None:
    if get_data_func == None:
        get_data_func = read_query

    data = get_data_func(
        "SELECT id, username, name, password, description, UIC, website, twitter, logo_id, status_id FROM companies WHERE "
        "username = ?",
        (username,),
    )
    return next((Company.from_query_result(*row) for row in data), None)


def find_by_id(company_id: int, get_data_func=None) -> Company | None:
    if get_data_func == None:
        get_data_func = read_query

    data = get_data_func(
        "SELECT id, username, name, password, description, UIC, website, twitter, logo_id, status_id FROM companies WHERE id = ?",
        (company_id,),
    )
    return next((Company.from_query_result(*row) for row in data), None)


def change_company_status(company_id: int, new_status: int, update_func=None):
    if update_func is None:
        update_func = update_query

    update_func(
        """UPDATE companies SET status_id = ? WHERE id = ?;""", (new_status, company_id)
    )
    return company_id
