from data.database import read_query, insert_query, update_query
from data.models.common_models import Skill, Status


def create(skill_name: str, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = insert_query

    generated_id = insert_data_func(
        """INSERT INTO skills (name) VALUES (?) """,
        (skill_name,),
    )

    return Skill(id=generated_id, name=skill_name, status_id=0)


def edit(old: Skill, new: Skill, update_data_func=None):
    if update_data_func is None:
        update_data_func = update_query

    merged = Skill(id=old.id, name=old.name or new.name, status_id=old.status_id)

    update_data_func(
        """UPDATE skills SET name = ? WHERE id = ?""",
        (merged.name,),
    )

    return merged


def delete(update_data_func=None):
    if update_data_func is None:
        update_data_func = update_query

    update_data_func(
        """DELETE FROM skills WHERE id = ?""",
        (id,),
    )


def all(get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func(
        """SELECT id, name, status_id FROM skills
        WHERE status_id !=9
        ORDER BY id """
    )

    return (
        Skill(id=id, name=name, status_id=status_id) for id, name, status_id in data
    )


def sort(lst: list[Skill], *, attribute="name", reverse=False):
    if attribute == "name":

        def sort_func(s: Skill):
            return s.name

    elif attribute == "id":

        def sort_func(s: Skill):
            return s.id

    return sorted(lst, key=sort_func, reverse=reverse)


def exist(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    return any(
        get_data_func(
            """SELECT id, name, status_id FROM skills WHERE id = ?""",
            (id,),
        )
    )


def find_by_name(name: str, get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func(
        """SELECT id, name, status_id FROM skills WHERE name is = ?""", (name,)
    )

    return next(
        Skill(id=id, name=name, status_id=status_id) for id, name, status_id in data
    )


def change_skill_status(
    skill_id: int, new_status: int = Status, update_func=None
):
    if update_func is None:
        update_func = update_query

    update_func(
        """UPDATE skills SET status_id = ? WHERE id = ?;""", (new_status, skill_id)
    )
    return skill_id
