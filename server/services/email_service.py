from mailjet_rest import Client
from common.config import settings

api_key = settings.EMAIL_API_KEY
api_secret = settings.EMAIL_API_SECRET


def notify_admin_for_new_job_ad(new_job_ad_id: int):
    mailjet = Client(auth=(api_key, api_secret), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {
                    "Email": "nikolay.likiov.a40@learn.telerikacademy.com",
                    "Name": "The Job Lander Team",
                },
                "To": [
                    {
                        "Email": "nikolay.likiov.a40@learn.telerikacademy.com",
                        "Name": "Niki",
                    },
                    {
                        "Email": "evgeni.vladimirov.a40@learn.telerikacademy.com",
                        "Name": "Evgeni",
                    },
                    {
                        "Email": "rossitsa.racheva.a40@learn.telerikacademy.com",
                        "Name": "Rossi",
                    },
                ],  # list of JSON pairs of recipients
                "TemplateID": 4359956,
                "TemplateLanguage": True,
                "Subject": (f"New JobAd with id:{new_job_ad_id} pending for approval!"),
                "Variables": {"job_ad_id": f"{new_job_ad_id}"},
            }
        ]
    }
    result = mailjet.send.create(data=data)

    print(result.status_code)
    print(result.json())


def notify_professional_for_new_match_request(company_name: str):
    mailjet = Client(auth=(api_key, api_secret), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {
                    "Email": "nikolay.likiov.a40@learn.telerikacademy.com",
                    "Name": "The Job Lander Team",
                },
                "To": [
                    {
                        "Email": "nikolay.likiov.a40@learn.telerikacademy.com",
                        "Name": "Niki",
                    },
                    {
                        "Email": "evgeni.vladimirov.a40@learn.telerikacademy.com",
                        "Name": "Evgeni",
                    },
                    {
                        "Email": "rossitsa.racheva.a40@learn.telerikacademy.com",
                        "Name": "Rossi",
                    },
                ],  # list of JSON pairs of recipients
                "TemplateID": 4352121,
                "TemplateLanguage": False,
                "Subject": f"You've got a new match request by company {company_name}",
                "Variables": {"company_name": f"{company_name}"},
            }
        ]
    }
    result = mailjet.send.create(data=data)

    print(result.status_code)
    print(result.json())


def notify_company_for_new_match_request(professional_name: str):
    mailjet = Client(auth=(api_key, api_secret), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {
                    "Email": "py.test.a40@gmail.com",
                    "Name": "The Job Lander Team",
                },
                "To": [
                    {
                        "Email": "nikolay.likiov.a40@learn.telerikacademy.com",
                        "Name": "Niki",
                    },
                    {
                        "Email": "evgeni.vladimirov.a40@learn.telerikacademy.com",
                        "Name": "Evgeni",
                    },
                    {
                        "Email": "rossitsa.racheva.a40@learn.telerikacademy.com",
                        "Name": "Rossi",
                    },
                ],  # list of JSON pairs of recipients
                "TemplateID": 4359947,
                "TemplateLanguage": False,
                "Subject": f"You've got a Job Match request from professional {professional_name}",
                "Variables": {"professional_name": f"{professional_name}"},
            }
        ]
    }
    result = mailjet.send.create(data=data)
    print(result.status_code)
    print(result.json())


def test_notify(professional_name: str):
    mailjet = Client(auth=(api_key, api_secret), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {
                    "Email": "py.test.a40@gmail.com",
                    "Name": "The Job Lander Team",
                },
                "To": [
                    {
                        "Email": "nikolay.likiov.a40@learn.telerikacademy.com",
                        "Name": "Niki",
                    },
                ],  # list of JSON pairs of recipients
                "TemplateID": 4359947,
                "TemplateLanguage": False,
                "Subject": f"Congs! You've got a Job Match request from professional {professional_name}",
                "Variables": {"professional_name": professional_name},
            }
        ]
    }
    result = mailjet.send.create(data=data)
    print(result.status_code)
    print(result.json())
