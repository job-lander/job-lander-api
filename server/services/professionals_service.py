from data.database import (
    read_query,
    _get_connection,
    insert_query_transactional,
    insert_query,
    update_query,
)
from common.security import get_password_hash
from data.database import (
    read_query,
    _get_connection,
    insert_query_transactional,
    insert_query,
    update_query,
)
from common.security import create_access_token, get_password_hash
from data.models.common_models import CreatedLocation, Status
from data.models.professional_models import (
    CreatedProfessional,
    Professional,
    ResponceModelProfessional,
)


def create(
    professional: CreatedProfessional,
    location: CreatedLocation,
    insert_professional_data_func=None,
    insert_country_id_func=None,
    insert_town_func=None,
    get_location_id_func=None,
):
    # if insert_country_id_func is None:
    #     insert_country_id_func = read_query
    # country_id = insert_country_id_func(
    #    """SELECT id from countries WHERE name = ?""", (location.country,)
    # )

    try:
        conn = _get_connection()

        if insert_country_id_func is None:
            insert_country_id_func = insert_query_transactional

        country_id = insert_country_id_func(
            """INSERT INTO countries SET name = ? ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (location.country,),
        )

        if insert_town_func is None:
            insert_town_func = insert_query_transactional
        town_id = insert_town_func(
            """INSERT into towns SET name = ? ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (location.town,),
        )

        if get_location_id_func is None:
            get_location_id_func = insert_query_transactional
        location_id = get_location_id_func(
            """INSERT into locations (town_id, country_id) VALUES (?,?) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id)""",
            conn,
            (
                town_id,
                country_id,
            ),
        )

        professional.password = get_password_hash(professional.password)

        if insert_professional_data_func is None:
            insert_professional_data_func = insert_query_transactional
        generated_professional_id = insert_professional_data_func(
            """INSERT INTO professionals (first_name, last_name, username, password, email,
        brief_summary, location_id, status_id, photo_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)""",
            conn,
            (
                professional.first_name,
                professional.last_name,
                professional.username,
                professional.password,
                professional.brief_summary,
                professional.email,
                location_id,
                9,  # unapproved
                1,  # default photo
            ),
        )

        conn.commit()

        # token = create_access_token({generated_professional_id: professional.username})

        professional = Professional(
            id=generated_professional_id,
            first_name=professional.first_name,
            last_name=professional.last_name,
            username=professional.username,
            password=professional.password,
            email=professional.email,
            brief_summary=professional.brief_summary,
            location_id=location_id,
            status_id=Status.UNAPPROVED,
            photo_id=1,
        )
        return professional

    except Exception as e:
        conn.rollback()
        return e.args
    finally:
        conn.close()


def edit_professional_info(
    old: ResponceModelProfessional,
    new: ResponceModelProfessional,
    professional_id: int,
    update_professional_data_func=None,
):
    if update_professional_data_func is None:
        update_professional_data_func = update_query

    merged = ResponceModelProfessional(
        id=old.id,
        first_name=old.first_name or new.first_name,
        last_name=old.last_name or new.last_name,
        username=new.username or old.username,
        password=old.password,
        email=new.email or old.email,
        brief_summary=new.brief_summary or old.brief_summary,
        town=old.town,
        country=old.country,
    )

    update_professional_data_func(
        """
        UPDATE professionals SET first_name = ?, last_name = ?, username = ?, password = ?, email = ?, brief_summary = ? 
        WHERE id = ?
        """,
        (
            merged.first_name,
            merged.last_name,
            merged.username,
            merged.password,
            merged.email,
            merged.brief_summary,
            professional_id,
        ),
    )

    return merged


def all(get_professional_data_func=None):
    if get_professional_data_func is None:
        get_professional_data_func = read_query

    data = get_professional_data_func(
        """
        SELECT p.id, p.first_name, p.last_name, p.username, p.password, p.email, p.brief_summary, t.name, c.name
        FROM professionals as p 
        JOIN locations as l on p.location_id = l.id
        JOIN countries as c ON l.country_id = c.id
        JOIN towns as t ON l.town_id = t.id
        """
    )

    return (
        ResponceModelProfessional(
            id=id,
            first_name=first_name,
            last_name=last_name,
            username=username,
            password=password,
            email=email,
            brief_summary=brief_summary,
            town=town,
            country=country,
        )
        for id, first_name, last_name, username, password, email, brief_summary, town, country in data
    )


def delete(username: str, update_professional_data_func=None):
    if update_professional_data_func is None:
        update_professional_data_func = update_query

    # look for token and user based on the token, and than look for username

    data = update_professional_data_func(
        """ UPDATE professionals SET status_id = 8 WHERE username = ?""", (username,)
    )


def sort(
    lst: list[Professional], *, attribute="first_name", reverse=False
):  # any other sort??
    if attribute == "first_name":

        def sort_func(p: Professional):
            return p.first_name

    elif attribute == "last_name":

        def sort_func(p: Professional):
            return p.last_name

    elif attribute == "username":

        def sort_func(p: Professional):
            return p.username

    return sorted(lst, key=sort_func, reverse=reverse)


def exist(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    return any(
        get_data_func(
            """SELECT id, first_name, last_name, username, brief_summary, location_id, status_id,
            photo_id FROM professionals WHERE id = ?""",
            (id,),
        )
    )


def find_by_username(username: str, get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    professional = get_data_func(
        """
            SELECT p.id, p.first_name, p.last_name, p.username, p.password, p.email, p.brief_summary, t.name, c.name
            FROM professionals as p 
            JOIN locations as l on p.location_id = l.id
            JOIN countries as c ON l.country_id = c.id
            JOIN towns as t ON l.town_id = t.id
            WHERE username = ?""",
        (username,),
    )

    return ResponceModelProfessional.from_query_result(*professional[0])


def find_by_id(professional_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    professional = get_data_func(
        """
            SELECT p.id, p.first_name, p.last_name, p.username, p.password, p.email, p.brief_summary, t.name as town, c.name as country
            FROM professionals as p 
            JOIN locations as l on p.location_id = l.id
            JOIN countries as c ON l.country_id = c.id
            JOIN towns as t ON l.town_id = t.id
            WHERE p.id = ?""",
        (professional_id,),
    )

    return ResponceModelProfessional.from_query_result(*professional[0])


def upload_photo(file: bytes, professional_id: int, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = insert_query
    generated_id = (
        insert_data_func("""INSERT INTO images (image) VALUES (?)""", (file,)),
    )

    return update_query(
        """UPDATE professionals SET photo_id = ? WHERE id = ?""",
        (
            generated_id[0],
            professional_id,
        ),
    )


def change_professional_status(
    professional_id: int, new_status: int = Status, update_func=None
):
    if update_func is None:
        update_func = update_query

    update_func(
        """UPDATE professionals SET status_id = ? WHERE id = ?;""",
        (new_status, professional_id),
    )
    return professional_id


def get_professional_cv_ads_stats(professional_id: int, get_data_func=None) -> dict:
    if get_data_func == None:
        get_data_func = read_query

    cv_ads = get_data_func(
        """SELECT status_id from cv_ads where professional_id = ?""",
        (professional_id,),
    )

    number_of_active_cv_ads = 0
    number_of_matched_cv_ads = 0
    if cv_ads:
        for ad in cv_ads:
            if ad[0] == Status.ACTIVE:
                number_of_active_cv_ads += 1
            elif ad[0] == Status.MATCHED:
                number_of_matched_cv_ads += 1

    return {
        "active_ads": number_of_active_cv_ads,
        "matched_ads": number_of_matched_cv_ads,
    }
