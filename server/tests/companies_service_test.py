from mariadb import IntegrityError
import unittest
from datetime import datetime
from unittest.mock import Mock
from common.security import get_password_hash
from data.models.company_models import (
    Company,
    CreatedCompany,
    Contacts,
    CompanyContacts,
    CreatedContacts,
)
from data.models.common_models import CreatedLocation
from services import companies_service

mock_companies_service = Mock(spec="services.companies_service")


def fake_company(
    id=1,
    username="Telefake",
    name="Telerik Faking",
    password="Kaserola23*",
    description="Fake description",
    UIC=1234567890,
    website="www.telefake.com",
    twitter="faketweet",
    logo_id=1,
    status_id=9,
):
    mock_company = Mock(spec=Company)
    mock_company.id = id
    mock_company.username = username
    mock_company.name = name
    mock_company.password = password
    mock_company.description = description
    mock_company.UIC = UIC
    mock_company.website = website
    mock_company.twitter = twitter
    mock_company.logo_id = logo_id
    mock_company.status_id = status_id
    return mock_company


company = fake_company()

company_query = [
    (
        1,
        "Telefake",
        "Telerik Faking",
        "Kaserola23*",
        "Fake description",
        1234567890,
        "www.telefake.com",
        "faketweet",
        1,
        9,
    )
]
dummy_company = Company(
    id=company.id,
    username=company.username,
    name=company.name,
    password=company.password,
    description=company.description,
    UIC=company.UIC,
    website=company.website,
    twitter=company.twitter,
    logo_id=company.logo_id,
    status_id=company.status_id,
)
dummy_created_company = CreatedCompany(
    username=company.username,
    name=company.name,
    password=company.password,
    description=company.description,
    UIC=company.UIC,
    website=company.website,
    twitter=company.twitter,
)
contacts_query = [
    (
        1,
        "address",
        "123456",
        "email",
        1,
        2,
    )
]
dummy_contacts = Contacts(
    id=1, address="address", phone="123456", email="email", location_id=1, company_id=2
)
company_contacts_query = [(1, "Sofia", "BG", "address", "123456", "email")]
dummy_company_contacts = CompanyContacts(
    id=1, town="Sofia", country="BG", address="address", phone="123456", email="email"
)
dummy_created_contacts = CreatedContacts(
    address="address", phone="123456", email="email"
)
dummy_created_location = CreatedLocation(town="Sofia", country="BG")
dummy_file = bytes("string", encoding="utf8")


class CompanyService_Should(unittest.TestCase):
    def test_CreateCompany_returnsCompany(self):
        get_country_id_func = lambda q, conn, params: [(1,)]
        insert_town_func = lambda q, conn, params: 2
        get_location_id_func = lambda q, conn, params: 3
        insert_contacts_data_func = lambda q, conn, params: 1
        result = companies_service.add_contact(
            1,
            dummy_created_contacts,
            dummy_created_location,
            get_country_id_func,
            insert_town_func,
            get_location_id_func,
            insert_contacts_data_func,
        )
        expected = dummy_company_contacts
        self.assertEqual(result, expected)

    def test_CreateCompany_returnsErrorOnOnvalidCountry(self):
        get_country_id_func = lambda q, conn, params: []
        insert_town_func = lambda q, conn, params: 2
        get_location_id_func = lambda q, conn, params: 3
        insert_contacts_data_func = lambda q, conn, params: 1
        result = companies_service.add_contact(
            1,
            dummy_created_contacts,
            dummy_created_location,
            get_country_id_func,
            insert_town_func,
            get_location_id_func,
            insert_contacts_data_func,
        )
        expected = ("No such country",)
        self.assertIsInstance(result, tuple)
        self.assertEqual(result, expected)

    def test_GetCompanyInfo_returnsCompanyInfo(self):
        get_company_func = lambda q, id: {
            "name": "Company",
            "description": "A nice company",
            "and_so_on": "we_get_it",
        }
        result = companies_service.get_company_info(1, get_company_func)

        self.assertIsInstance(result, dict)

    def test_GetCompanyInfo_returnsNoneWhenNotFound(self):
        get_company_func = lambda q, id: None
        result = companies_service.get_company_info(1, get_company_func)
        expected = None
        self.assertEqual(result, expected)

    def test_AddCompanyContact_returnsCompanyContacts(self):
        get_country_id_func = lambda q, conn, params: [(1,)]
        insert_town_func = lambda q, conn, params: 2
        get_location_id_func = lambda q, conn, params: 3
        insert_contacts_data_func = lambda q, conn, params: 1
        result = companies_service.add_contact(
            1,
            dummy_created_contacts,
            dummy_created_location,
            get_country_id_func,
            insert_town_func,
            get_location_id_func,
            insert_contacts_data_func,
        )
        expected = dummy_company_contacts
        self.assertEqual(result, expected)

    def test_AddCompanyContact_returnsErroronInvalidCountry(self):
        get_country_id_func = lambda q, conn, params: []
        insert_town_func = lambda q, conn, params: 2
        get_location_id_func = lambda q, conn, params: 3
        insert_contacts_data_func = lambda q, conn, params: 1
        result = companies_service.add_contact(
            1,
            dummy_created_contacts,
            dummy_created_location,
            get_country_id_func,
            insert_town_func,
            get_location_id_func,
            insert_contacts_data_func,
        )
        expected = ("No such country",)
        self.assertIsInstance(result, tuple)
        self.assertEqual(result, expected)

    def test_EditCompanyContacts_returnsCompanyContacts(self):
        get_old_contact_func = lambda q, params: {
            "company_id": 1,
            "address": "address",
            "phone": "123456",
            "email": "email",
            "town_name": "Sofia",
            "country_name": "BG",
            "town_id": 2,
            "country_id": 1,
        }
        get_country_id_func = lambda q, params: 1
        insert_town_func = lambda q, conn, params: 2
        get_location_id_func = lambda q, conn, params: 3
        update_contacts_data_func = lambda q, params: company_contacts_query
        result = companies_service.edit_contact(
            1,
            dummy_created_contacts,
            dummy_created_location,
            get_old_contact_func,
            get_country_id_func,
            insert_town_func,
            get_location_id_func,
            update_contacts_data_func,
        )
        expected = dummy_company_contacts
        self.assertEqual(result, expected)

    def test_GetContacts_returnsCompanyContacts(self):
        get_contacts_func = lambda q, id: company_contacts_query
        result = list(companies_service.get_contacts(1, get_contacts_func))
        expected = dummy_company_contacts
        self.assertEqual(result[0], expected)

    def test_GetContacts_returnsMultipleCompanyContacts(self):
        get_contacts_func = lambda q, id: [
            company_contacts_query[0],
            company_contacts_query[0],
        ]
        result = list(companies_service.get_contacts(1, get_contacts_func))
        data = [company_contacts_query[0], company_contacts_query[0]]
        expected = list(CompanyContacts.from_query_result(*row) for row in data)
        self.assertEqual(result[0], expected[0])

    def test_GetContacts_returnsEmptyList(self):
        get_contacts_func = lambda q, id: []
        result = companies_service.get_contacts(1, get_contacts_func)
        expected = []
        self.assertEqual(result, expected)

    def test_GetContactsbyID_returnsContacts(self):
        get_contacts_func = lambda q, id: contacts_query
        result = companies_service.get_contact_by_id(1, 2, get_contacts_func)
        expected = dummy_contacts
        self.assertIsInstance(result, Contacts)
        self.assertEqual(result, expected)

    def test_GetContactsbyID_returnsNone(self):
        get_contacts_func = lambda q, id: []
        result = companies_service.get_contact_by_id(1, 2, get_contacts_func)
        expected = None
        self.assertEqual(result, expected)

    def test_GetCompanyJobAdsStats_returnsStats(self):
        get_data_func = lambda q, id: [(1,), (1,), (1,), (5,), (5,)]
        result = companies_service.get_company_job_ads_stats(1, get_data_func)
        expected = {
            "active_ads": 3,
            "matched_ads": 2,
        }
        self.assertEqual(result, expected)

    def test_upload_logo(self):
        insert_data_func = lambda file, id: [1]
        update_logo_func = lambda logo_id, company_id: True
        result = companies_service.upload_logo(
            insert_data_func=insert_data_func,
            update_logo_func=update_logo_func,
            file=dummy_file,
            company_id=1,
        )
        self.assertEqual(True, result)

    def test_EditCompanyInfo_EditsInfoCorrectly(self):
        update_info_func = lambda query, params: company_query
        result = companies_service.edit_company_info(
            dummy_company,
            dummy_created_company,
            update_info_func,
        )
        self.assertIsInstance(result, CreatedCompany)

    def test_FindByUsername_ReturnsCompanyOnCorrectUsername(self):
        get_data_func = lambda query, id: company_query
        result = companies_service.find_by_username(company.username, get_data_func)
        expected = dummy_company
        self.assertIsInstance(result, Company)
        self.assertEqual(result, expected)

    def test_FindByUsername_ReturnsNoneOnWrongUsername(self):
        get_data_func = lambda query, id: []
        result = companies_service.find_by_username(company.id, get_data_func)
        expected = None
        self.assertEqual(result, expected)

    def test_FindById_ReturnsCompanyOnCorrectID(self):
        get_data_func = lambda query, id: company_query
        result = companies_service.find_by_id(company.id, get_data_func)
        expected = dummy_company
        self.assertIsInstance(result, Company)
        self.assertEqual(result, expected)

    def test_FindById_ReturnsNoneOnWrongID(self):
        get_data_func = lambda query, id: []
        result = companies_service.find_by_id(company.id, get_data_func)
        expected = None
        self.assertEqual(result, expected)

    def test_changeCompanyStatus_changes_status(self):
        update_func = lambda q, params: True
        result = companies_service.change_company_status(1, 1, update_func)
        expected = True
        self.assertEqual(result, expected)
