import unittest
from unittest.mock import Mock

from common.responses import BadRequest, NotFound
from data.models.company_models import (
    Company,
    CompanyContacts,
    CompanyInfo,
    CreatedCompany,
)
from companies_service_test import (
    dummy_company,
    dummy_created_company,
    dummy_created_contacts,
    dummy_created_location,
    dummy_company_contacts,
    dummy_file,
)
from routers.companies import companies as companies_router

mock_company_service = Mock(spec="services.companies_service")
mock_match_request_service = Mock(spec="services.match_request_service")
mock_jobad_service = Mock(spec="services.job_ads_service")
mock_id_from_token = Mock(spec="services.admin.admin_service")

companies_router.match_request_service = mock_match_request_service
companies_router.companies_service = mock_company_service
companies_router.get_id_from_token = mock_id_from_token


def fake_company(
    id=1,
    username="Telefake",
    name="Telerik Faking",
    password="Kaserola23*",
    description="Fake description",
    UIC=1234567890,
    website="www.telefake.com",
    twitter="faketweet",
    logo_id=1,
    status_id=9,
):

    mock_company = Mock(spec=Company)
    mock_company.id = id
    mock_company.username = username
    mock_company.name = name
    mock_company.password = password
    mock_company.description = description
    mock_company.UIC = UIC
    mock_company.website = website
    mock_company.twitter = twitter
    mock_company.logo_id = logo_id
    mock_company.status_id = status_id
    return mock_company


dummy_company_info = CompanyInfo(
    id=1,
    name="Company",
    description="Company Description",
    UIC=1234567890,
    website="website",
    twitter=None,
    logo=dummy_file,
    active_job_ads=0,
    number_of_successful_matches=0,
    contacts=[],
)
company = fake_company()


class CompanyRouter_Should(unittest.TestCase):
    def setUp(self) -> None:
        mock_company_service.reset_mock()

    def test_Register_returnsCompanyOnCorrectData(self):
        created_company = dummy_created_company
        contacts = dummy_created_contacts
        location = dummy_created_location
        created = mock_company_service.create = lambda x, y, z: company

        result = companies_router.register(created_company, contacts, location)
        expected = company

        self.assertEqual(result, expected)

    def test_Register_returnsErrorOnInvalidData(self):
        created_company = dummy_created_company
        contacts = dummy_created_contacts
        location = dummy_created_location
        created = mock_company_service.create = lambda x, y, z: Exception()

        result = type(companies_router.register(created_company, contacts, location))
        expected = type(BadRequest())

        self.assertEqual(result, expected)

    def test_AddContact_returnsMessageOnSuccessfulAdd(self):
        contacts = dummy_created_contacts
        location = dummy_created_location
        mock_company_service.find_by_id = lambda x: 1
        mock_company_service.add_contact = (
            lambda id, contacts, location: dummy_company_contacts
        )
        result = companies_router.add_contact(contacts, location)

        self.assertIsInstance(result, str)

    def test_AddContact_returnsBadRequestOnInvalidIdFromToken(self):
        contacts = dummy_created_contacts
        location = dummy_created_location
        mock_company_service.find_by_id = lambda x: None
        mock_company_service.add_contact = (
            lambda id, contacts, location: dummy_company_contacts
        )
        result = type(companies_router.add_contact(contacts, location))
        expected = type(BadRequest())
        self.assertEqual(result, expected)

    def test_AddContact_returnsBadRequestOnInvalidContactsData(self):
        contacts = dummy_created_contacts
        location = dummy_created_location
        mock_company_service.find_by_id = lambda x: 1
        mock_company_service.add_contact = (
            lambda id, contacts, location: "something_else"
        )
        result = type(companies_router.add_contact(contacts, location))
        expected = type(BadRequest())
        self.assertEqual(result, expected)

    def test_EditContact_returnsCompanyContactOnSuccessfulEdit(self):
        contacts = dummy_created_contacts
        location = dummy_created_location
        mock_company_service.find_by_id = lambda x: 1
        mock_company_service.get_contact_by_id = lambda x, y: 1
        mock_company_service.edit_contact = (
            lambda id, contacts, location: dummy_company_contacts
        )
        result = companies_router.update_contact(1, contacts, location)
        self.assertIsInstance(result, CompanyContacts)

    def test_EditContact_returnsBadRequestOnInvalidIdFromToken(self):
        contacts = dummy_created_contacts
        location = dummy_created_location
        mock_company_service.find_by_id = lambda x: None
        mock_company_service.get_contact_by_id = lambda x, y: 1
        mock_company_service.edit_contact = (
            lambda id, contacts, location: dummy_company_contacts
        )
        result = type(companies_router.update_contact(1, contacts, location))
        expected = type(BadRequest())
        self.assertEqual(result, expected)

    def test_EditContact_returnsBadRequestOnInvalidContactId(self):
        contacts = dummy_created_contacts
        location = dummy_created_location
        mock_company_service.find_by_id = lambda x: 1
        mock_company_service.get_contact_by_id = lambda x, y: None
        mock_company_service.edit_contact = (
            lambda id, contacts, location: dummy_company_contacts
        )
        result = type(companies_router.update_contact(1, contacts, location))
        expected = type(BadRequest())
        self.assertEqual(result, expected)

    def test_EditContact_returnsBadRequestOnInvalidContactUpdate(self):
        contacts = dummy_created_contacts
        location = dummy_created_location
        mock_company_service.find_by_id = lambda x: 1
        mock_company_service.get_contact_by_id = lambda x, y: 2
        mock_company_service.edit_contact = lambda id, contacts, location: Exception()
        result = type(companies_router.update_contact(1, contacts, location))
        expected = type(Exception())
        self.assertEqual(result, expected)

    def test_ViewCompanyInfo_returnsCompanyInfo(self):
        mock_company_service.get_company_info = lambda x: {
            "name": "name",
            "image": "logostring",
            "description": "description",
            "UIC": 1234567890,
            "website": "website",
            "twitter": "twitter",
            "company_id": 1,
            "active_ads": 3,
            "matched_ads": 30,
        }
        mock_company_service.get_company_job_ads_stats = lambda x: {
            "active_ads": 3,
            "matched_ads": 30,
        }
        mock_company_service.get_contacts = lambda id: [dummy_company_contacts]
        result = companies_router.view_company_info(1)
        self.assertIsInstance(result, CompanyInfo)

    def test_UploadLogo_UploadsSuccessfully(self):
        mock_company_service.find_by_id = lambda x: 1
        mock_company_service.upload_logo = lambda id, file: True
        result = companies_router.upload_logo(dummy_file)
        expected = (
            f"Logo of company with id:Depends(get_id_from_token) successfully updated"
        )
        self.assertEqual(result, expected)

    def test_UploadLogo_ReturnsBadRequestOnInvalidIdFromToken(self):
        mock_company_service.find_by_id = lambda x: None
        mock_company_service.upload_logo = lambda id, file: True
        result = type(companies_router.upload_logo(dummy_file))
        expected = type(BadRequest())
        self.assertEqual(result, expected)

    def test_UploadLogo_ReturnsMessageIfNoFileUploaded(self):
        mock_company_service.find_by_id = lambda x: 1
        mock_company_service.upload_logo = lambda id, file: False
        result = companies_router.upload_logo(file=None)
        expected = {"message": "No file sent"}
        self.assertEqual(result, expected)

    def test_EditCompanyInfo_ReturnsUpdatedInfo(self):
        mock_company_service.find_by_id = lambda x: 1
        mock_company_service.edit_company_info = (
            lambda company, info: dummy_created_company
        )
        result = type(
            companies_router.edit_company_info(dummy_company, dummy_company_contacts)
        )
        expected = type(dummy_created_company)
        self.assertEqual(result, expected)

    def test_EditCompanyInfo_ReturnsBadRequestOnInvalidIdFromToken(self):
        mock_company_service.find_by_id = lambda x: None
        mock_company_service.edit_company_info = (
            lambda company, info: dummy_created_company
        )
        result = type(
            companies_router.edit_company_info(dummy_company, dummy_company_contacts)
        )
        expected = type(BadRequest())
        self.assertEqual(result, expected)

    def test_SendMatchRequest_toCVad_ReturnsMessageOnSuccessfulMatchRequest(self):
        mock_company_service.find_by_id = lambda x: dummy_company
        mock_match_request_service.match_request_company_to_cvad = (
            lambda company_id, cvad_id, company_name: 1
        )
        result = companies_router.send_match_request_to_cvad(1)
        self.assertIsInstance(result, str)

    def test_SendMatchRequest_toCVad_ReturnsBadRequestOnInvalidIdFromToken(self):
        mock_company_service.find_by_id = lambda x: None
        mock_match_request_service.match_request_company_to_cvad = (
            lambda company_id, cvad_id, company_name: 1
        )
        result = type(
            companies_router.edit_company_info(dummy_company, dummy_company_contacts)
        )
        expected = type(BadRequest())
        self.assertEqual(result, expected)

    def test_SendMatchRequest_toCVadViaJobAd_ReturnsMessageOnSuccessfulMatchRequest(
        self,
    ):
        mock_company_service.find_by_id = lambda x: dummy_company
        mock_match_request_service.match_request_company_jobad_to_cvad = (
            lambda jobad_id, cvad_id, company_name: 1
        )
        result = companies_router.send_match_request_to_cvad_via_job_ad(1, 1)
        self.assertIsInstance(result, str)

    def test_SendMatchRequest_toCVadViaJobAd_ReturnsBadRequestOnInvalidIdFromToken(
        self,
    ):
        mock_company_service.find_by_id = lambda x: None
        mock_match_request_service.match_request_company_jobad_to_cvad = (
            lambda jobad_id, cvad_id, company_name: 1
        )
        result = type(companies_router.send_match_request_to_cvad_via_job_ad(1, 1))
        expected = type(BadRequest())
        self.assertEqual(result, expected)
