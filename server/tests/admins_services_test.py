import unittest
from unittest.mock import Mock, patch

from data.models.common_models import TUsername, TPassword, TEmail
from data.models.admin_models import AdminIn, AdminFromDB

from services import admins_service


def fake_adminIn(
	username: TUsername,
	password: TPassword,
	first_name: str,
	last_name: str,
	email: TEmail
):
	
	mock_admin = Mock(spec=AdminIn)
	mock_admin.username = username
	mock_admin.password = password
	mock_admin.first_name = first_name
	mock_admin.last_name = last_name
	mock_admin.email = email
	
	return mock_admin


def fake_adminFromDB(
	admin_id: int,
	username: str,
	password: str,
	first_name: str,
	last_name: str,
	email: str
):
	
	mock_adminfrom_db = Mock(spec=AdminFromDB)
	mock_adminfrom_db.id = admin_id
	mock_adminfrom_db.username = username
	mock_adminfrom_db.password = password
	mock_adminfrom_db.first_name = first_name
	mock_adminfrom_db.last_name = last_name
	mock_adminfrom_db.email = email
	
	return mock_adminfrom_db


def fake_token():
	mock_token = Mock(spec=str)
	return mock_token


mock_id_from_token = Mock(spec="services.admin.admins_service")
admins_service.get_id_from_token = mock_id_from_token


class AdminsService_Should(unittest.TestCase):
	
	def test_createAdmin_returns_id_whenCorrectAdminInGiven(self):
		# Arrange:
		insert_func = lambda q, p: 1
		expected = 1
		
		# Act:
		mock_admin = fake_adminIn(
			username='rambo_admin', password='Rambo123*',
			first_name='rambo', last_name='stallone',
			email='rambo@rambo.com'
		)
		result = admins_service.create_admin(mock_admin, insert_func)
		
		# Assert:
		self.assertEqual(expected, result)
	
	def test_createAdmin_returns_IntegrityError_whenUsernameInDB(self):
		# Arrange:
		insert_func = lambda q, p: None
		expected = None
		
		# Act:
		mock_admin = fake_adminIn(
			username='rambo_admin', password='Rambo123*',
			first_name='rambo', last_name='stallone',
			email='rambo_not_in_db@rambo.com'
		)
		result = admins_service.create_admin(mock_admin, insert_func)
		
		# Assert:
		self.assertEqual(expected, result)
	
	def test_createAdmin_returns_IntegrityError_whenEmailInDB(self):
		# Arrange:
		insert_func = lambda q, p: None
		expected = None
		
		# Act:
		mock_admin = fake_adminIn(
			username='rambo_admin_not_in_db', password='Rambo123*',
			first_name='rambo', last_name='stallone',
			email='rambo@rambo.com'
		)
		result = admins_service.create_admin(mock_admin, insert_func)
		
		# Assert:
		self.assertEqual(expected, result)
	
	def test_find_by_username_returns_AdminFromDB(self):
		# Arrange & Act
		username = "rambo_admin"
		read_func = lambda q, p: [(1, 'rambo_admin',
		                           'fake_pass',
		                           'rambo', 'stallone', 'rambo@rambo.com')]
		expected = AdminFromDB(
			id=1,
			username='rambo_admin', password='fake_pass',
			first_name='rambo', last_name='stallone',
			email='rambo@rambo.com'
		)
		actual = admins_service.find_by_username(username=username, read_func=read_func)
		
		# Assert
		self.assertEqual(expected, actual)
	
	def test_find_by_username_returns_None_ifNotInAdmins(self):
		# Arrange & Act
		username = "rambo_not_in_db"
		read_func = lambda q, p: []
		expected = None
		
		actual = admins_service.find_by_username(username=username, read_func=read_func)
		
		# Assert
		self.assertEqual(expected, actual)
	
	# def test_check_if_admin_returns_True_whenIdFromTokenInDB(self):
	# 	read_func = lambda q, p: 1
	# 	token = fake_token()
	#
	# 	expected = True
	#
	# 	actual = admins_service.check_if_admin(token=token, read_func=read_func)
	#
	# 	self.assertEqual(expected, actual)
