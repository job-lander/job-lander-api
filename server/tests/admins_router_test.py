import unittest
from unittest.mock import Mock
from routers.admins.admins import admins_router

from data.models.admin_models import AdminIn, AdminOut
from data.models.common_models import TUsername, TPassword, TEmail


mock_admins_service = Mock('services.admins_service')
admins_router.admins_service = mock_admins_service


def fake_adminIn(
	username: TUsername,
	password: TPassword,
	first_name: str,
	last_name: str,
	email: TEmail
	):
	
	mock_admin = Mock(spec=AdminIn)
	mock_admin.username = username
	mock_admin.password = password
	mock_admin.first_name = first_name
	mock_admin.last_name = last_name
	mock_admin.email = email
	
	return mock_admin

class AdminsRouter_Should(unittest.TestCase):
	
	def setUp(self) -> None:
		mock_admins_service.reset_mock()
		
	def test_registerAdmin_returns_AdminOut_whenSuccessful(self):
	