import uvicorn
from fastapi import FastAPI
from routers.admins.admins import admins_router
from routers.admins.companies_admins import companies_admins_router
from routers.admins.professionals_admins import professionals_admins_router
from routers.admins.skills_admins import skills_admins_router
from routers.auth.login import login_router
from routers.companies.companies import companies_router
from routers.professionals.cv_ads import CVs_router
from routers.companies.job_ads import job_ads_router
from routers.professionals.professionals import professionals_router
from routers.skills import skills_router
from routers.images import images_router
from tkinter import ttk
from tkinter import *


app = FastAPI(title="Job Lander")
app.include_router(login_router, tags=["login"])
app.include_router(admins_router, tags=['admins'])
app.include_router(companies_admins_router, tags=["admins_companies"])
app.include_router(professionals_admins_router, tags=["admins_professionals"])
app.include_router(skills_admins_router, tags=["admins_skills"])
app.include_router(companies_router, tags=["companies"])
app.include_router(CVs_router, tags=["company_ads"])
app.include_router(job_ads_router, tags=["job_ads"])
app.include_router(professionals_router, tags=["professionals"])
app.include_router(skills_router, tags=["skills"])
app.include_router(images_router, tags=["images"])


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=False)
