from fastapi import APIRouter
from services import skill_service

skills_router = APIRouter(prefix="/skills")


@skills_router.get("")
def get_skills(sort: str | None = None):

    skills = skill_service.all()

    if sort and (sort == "asc" or sort == "desc"):
        return skill_service.sort(skills, reverse=sort == "desc")
    return skills


# заради authentication-а вероятно трябва да добавим два POST ендпойнта - от professional и от company
@skills_router.post("")
def create_skill(skill_name: str):

    created_skill = skill_service.create(skill_name)

    return f"New skill - {created_skill.name}, was successfully created."
