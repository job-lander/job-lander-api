from fastapi import APIRouter, Header
from common.responses import NotFound, Unauthorized
from data.models.common_models import LogIn
from services import professionals_service, companies_service, admins_service

from common.security import pwd_context
from common.security import create_access_token, decode_access_token
from common.security import check_if_admin

login_router = APIRouter(prefix="/login")


@login_router.post("/professionals")
def login_prof(login: LogIn):
    username = login.username
    user = professionals_service.find_by_username(username)

    if not user:
        return NotFound()

    # check if pass is correct
    if not pwd_context.verify(login.password, user.password):
        return NotFound()

    # returns token
    token = create_access_token({"id": user.id, "username": user.username})
    return {"token": token}


@login_router.post("/companies")
def login_comp(login: LogIn):
    username = login.username
    company = companies_service.find_by_username(username)

    if not company:
        return NotFound()

    # check if pass is correct
    if not pwd_context.verify(login.password, company.password):
        return NotFound()

    # returns token
    token = create_access_token({"id": company.id, "username": company.username})
    return {"token": token}


@login_router.post("/admins")
def login_admin(login: LogIn):
    username = login.username
    user = admins_service.find_by_username(username)

    if not user:
        return NotFound()

    # check if pass is correct
    if not pwd_context.verify(login.password, user.password):
        return NotFound()

    # returns token
    token = create_access_token({"id": user.id, "username": user.username})
    return {"token": token}


# only for testing purposes
@login_router.post("/admins/token/")
def test_verify_token(token: str = Header()):
    if check_if_admin(token):
        decoded = decode_access_token(token)
        return {"decoded token": decoded}
    return Unauthorized
