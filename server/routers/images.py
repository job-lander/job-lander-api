from fastapi import APIRouter, File
from services import image_service

images_router = APIRouter(prefix="/images")

# Should I create different endpoints for uploading different images - for professional, for company logo etc?!?
@images_router.post("/")
def create_file(file: bytes | None = File(default=None)):
    if not file:
        return {"message": "No file sent"}
    else:
        image_service.upload(file)
        return {"file_size": len(file)}

