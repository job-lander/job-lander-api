from fastapi import APIRouter, Depends
from common.security import validate_token
from data.models.common_models import CreatedLocation, CreatedSkill, Status
from data.models.company_models import CreatedJobAd, JobAd, JobAdResponseModel
from services import job_ads_service, companies_service
from services.admins_service import get_id_from_token, check_if_admin
from common.responses import BadRequest, NotFound, Unauthorized

job_ads_router = APIRouter(prefix="/job_ads")
# job_ads_router = APIRouter(prefix="/job_ads", dependencies=[Depends(check_if_owner)])


@job_ads_router.post("/company/")
def create(
    ad: CreatedJobAd,
    skills: list[CreatedSkill],
    location: CreatedLocation,
    company_id: int = Depends(get_id_from_token),
) -> JobAd:
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    job_ad_or_error = job_ads_service.create(ad, skills, location, company_id)
    if isinstance(job_ad_or_error, JobAd):
        return f"Job Ad with id:{job_ad_or_error.id} sucessfully created for company {company.name}"
    else:
        return BadRequest(content=(f"{job_ad_or_error}"))


# Get all job ads with specified status: active, matched or archived
@job_ads_router.get("/company/")
def all_job_ads_by_company(
    job_status: int,
    company_id: int = Depends(get_id_from_token),
) -> list[JobAdResponseModel]:
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    if job_status == Status.ACTIVE:
        ads = job_ads_service.all(company_id, Status.ACTIVE)
        if not ads:
            return NotFound()
        return ads
    elif job_status == Status.MATCHED:
        ads = job_ads_service.all(company_id, Status.MATCHED)
        if not ads:
            return NotFound()
        return ads
    elif job_status == Status.ARCHIVED:
        ads = job_ads_service.all(company_id, Status.ARCHIVED)
        if not ads:
            return NotFound()
        return ads
    else:
        return BadRequest("Invalid job_status id")


@job_ads_router.get("/{job_ad_id}")
def find_job_ad_by_id(job_ad_id: int) -> JobAdResponseModel:
    job_ad = job_ads_service.find_by_id(job_ad_id)
    if not job_ad:
        return NotFound()
    if job_ad.status_id != Status.ACTIVE:
        return BadRequest(f"Job ad found, but it is with status: {job_ad.status_id}")
    return job_ad


@job_ads_router.put("/{job_ad_id}/approve")
def admin_approve_job_ad(job_ad_id: int, is_admin: bool = Depends(check_if_admin)):
    job_ad = job_ads_service.find_by_id(job_ad_id)
    if not job_ad:
        return BadRequest(f"Job Ad with id:{job_ad_id} does not exist")
    if is_admin:
        response = job_ads_service.approve_job_ad(job_ad_id)
        if type(response) == int:
            return f"Job Ad with id {job_ad_id} successfully approved"
    return Unauthorized()


@job_ads_router.put("/{job_ad_id}/activate")
def make_job_active(job_ad_id: int, company_id: int = Depends(get_id_from_token)):
    job_ad = job_ads_service.find_by_id(job_ad_id)
    if not job_ad:
        return BadRequest(f"Job Ad with id:{job_ad_id} does not exist")
    if job_ad.company_id != company_id:
        return BadRequest(
            f"Job Ad with id:{job_ad_id} does not belong to company with id:{company_id}"
        )
    if job_ad.status_id == Status.APPROVED or Status.ARCHIVED:
        updated_job_ad_id = job_ads_service.activate_job_ad(job_ad_id)
    else:
        return BadRequest(f"Job ad found, but it is with status {job_ad.status_id}")

    return f"Job ad with id:{updated_job_ad_id} successfully activated"


@job_ads_router.put("/{job_ad_id}")
def edit_job_ad_by_id(job_ad_id: int) -> JobAdResponseModel:
    pass
    # job_ad = job_ads_service.find_by_id(job_ad_id)
    # if not job_ad:
    #     return NotFound()
    # return job_ad


@job_ads_router.post("/{job_ad_id}/skills")
def add_skill_to_job_ad(skill: CreatedSkill, job_ad_id: int):
    job_ad = job_ads_service.find_by_id(job_ad_id)
    if not job_ad:
        return BadRequest(f"Job Ad with id:{job_ad_id} does not exist")
    updated_job_ad = job_ads_service.add_skill_to_ad(skill, job_ad_id, job_ad)
    if not isinstance(updated_job_ad, JobAdResponseModel):
        return BadRequest(updated_job_ad[0])
    return f"Skill {skill.name} successfully added to job_ad with id{job_ad_id}"


@job_ads_router.get("/")
def get_by_criteria(
    title: str | None = None,
    description: str | None = None,
    salary_low: int | None = None,
    salary_high: int | None = None,
    salary_treshold: int | None = None,  # whole number for percentage. Eg. 20 for 20%
    company_id: int | None = None,
    skills: list[CreatedSkill] | None = None,
    skills_number_treshold: int = 0,  # ability to lower the matching critedia by number of skills
    is_remote: int | None = None,
    status_id: int | None = Status.ACTIVE,
    town: str | None = None,
    country: str | None = None,
    authorized=Depends(validate_token),
) -> list[JobAdResponseModel]:
    job_ad = job_ads_service.find_by_criteria(
        title,
        description,
        salary_low,
        salary_high,
        salary_treshold,
        company_id,
        skills,
        skills_number_treshold,
        is_remote,
        status_id,
        town,
        country,
    )
    if not job_ad:
        return NotFound()
    return job_ad
