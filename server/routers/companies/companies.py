from fastapi import APIRouter, File, Depends
from data.models.common_models import CreatedLocation
from data.models.company_models import (
    CreatedCompany,
    CreatedContacts,
    Company,
    CompanyInfo,
    CompanyContacts,
)
from services import companies_service, match_request_service
from services.admins_service import get_id_from_token
from common.responses import BadRequest

companies_router = APIRouter(prefix="/companies")


@companies_router.post("/register")
def register(
    company: CreatedCompany,
    contacts: CreatedContacts,
    location: CreatedLocation,
):
    company_or_error = companies_service.create(company, contacts, location)
    if isinstance(company_or_error, Company):
        return company_or_error
    else:
        return BadRequest(content=(f"{company_or_error}"))


@companies_router.post("/contacts")
def add_contact(
    contacts: CreatedContacts,
    location: CreatedLocation,
    company_id: int = Depends(get_id_from_token),
):
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")

    result = companies_service.add_contact(company_id, contacts, location)
    if type(result) == CompanyContacts:
        return f"A new location in {location.town},{location.country} successfully added to company with id: {company_id}"
    return BadRequest(content=(f"{result}"))


# Edit company office
@companies_router.put("/contacts")
def update_contact(
    contact_id: int,
    new_contact: CreatedContacts,
    new_location: CreatedLocation,
    company_id: int = Depends(get_id_from_token),
):
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    contact = companies_service.get_contact_by_id(company_id, contact_id)
    if not contact:
        return BadRequest(
            f"Company with id:{company_id} does not have a contact with id:{contact_id}"
        )
    return companies_service.edit_contact(contact_id, new_contact, new_location)


@companies_router.get("/info")
def view_company_info(company_id=Depends(get_id_from_token)):
    company_info = companies_service.get_company_info(company_id)
    job_ads = companies_service.get_company_job_ads_stats(company_id)
    contacts = companies_service.get_contacts(company_id)

    logo = company_info["image"]

    return CompanyInfo(
        id=company_id,
        name=company_info["name"],
        description=company_info["description"],
        UIC=company_info["UIC"],
        website=company_info["website"],
        twitter=company_info["twitter"],
        logo=(f"Logo size: {(len(logo)/1000)}Kb"),
        active_job_ads=job_ads["active_ads"],
        number_of_successful_matches=job_ads["matched_ads"],
        contacts=contacts,
    )


@companies_router.post("/logo")
def upload_logo(
    file: bytes | None = File(default=None),
    company_id: int = Depends(get_id_from_token),
):
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    if not file:
        return {"message": "No file sent"}

    uploaded_logo = companies_service.upload_logo(file, company_id)
    if uploaded_logo:
        return f"Logo of company with id:{company_id} successfully updated"


@companies_router.put("/info/{company_id}")
def edit_company_info(
    data: CompanyContacts, company_id: int = Depends(get_id_from_token)
):
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    return companies_service.edit_company_info(company, data)


@companies_router.post("/match_requests/cv/{cvad_id}")
def send_match_request_to_cvad(
    cvad_id: int,
    company_id: int = Depends(get_id_from_token),
):
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    response = match_request_service.match_request_company_to_cvad(
        company_id, cvad_id, company.name
    )
    if type(response) == int:
        return f"Match request sucessfully sent by company {company.name} to CV ad with id:{cvad_id}"


@companies_router.post("/match_requests/ad_to_cv/{job_ad_id}/{cv_ad_id}")
def send_match_request_to_cvad_via_job_ad(
    job_ad_id: int,
    cv_ad_id: int,
    company_id: int = Depends(get_id_from_token),
):
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    response = match_request_service.match_request_company_jobad_to_cvad(
        job_ad_id, cv_ad_id, company.name
    )
    if type(response) == int:
        return f"Match request sucessfully sent by company {company.name} with job_ad:{job_ad_id} to CV ad with id:{cv_ad_id}"


@companies_router.get("/match_requests/received")
def view_received_match_requests(
    job_ad_id: int | None = None,
    company_id: int = Depends(get_id_from_token),
):
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    return match_request_service.view_match_requests_sent_to_company(
        company_id, job_ad_id
    )


@companies_router.get("/match_requests/sent")
def view_sent_match_requests(
    job_ad_id: int | None = None,
    company_id: int = Depends(get_id_from_token),
):

    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    return match_request_service.view_match_requests_sent_by_company(
        company_id, job_ad_id
    )


@companies_router.post("/match_requests/confirm/cv_to_ad/{match_request_id}")
def confirm_match_request_cv_to_ad(
    match_request_id: int,
    archive_job_ad: bool,
    company_id: int = Depends(get_id_from_token),
):
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    response = match_request_service.company_confirm_match_request_cv_to_ad(
        company_id, match_request_id, archive_job_ad
    )

    if response == "Invalid match request":
        return BadRequest(
            f"Match request with id:{match_request_id} does not belong to company with id:{company_id}"
        )
    elif type(response) == dict:
        professional_username = response["professional"]
        cv_ad_id = response["cv_ad_id"]
        return f"Congratulations on company {company.name} on making a match with professional with username {professional_username} via his/hers CV with id:{cv_ad_id}"
    else:
        return response


@companies_router.post("/match_requests/confirm/professional_to_ad/{match_request_id}")
def confirm_match_request_professional_to_ad(
    match_request_id: int,
    archive_job_ad: bool | None = None,
    company_id: int = Depends(get_id_from_token),
):
    company = companies_service.find_by_id(company_id)
    if not company:
        return BadRequest(f"Company with id:{company_id} does not exist")
    response = match_request_service.company_confirm_match_request_professional_to_ad(
        company_id, match_request_id, archive_job_ad
    )
    if response == "Invalid match request":
        return BadRequest(
            f"Match request with id:{match_request_id} does not belong to company with id:{company_id}"
        )
    elif type(response) == dict:
        professional_username = response["professional"]
        return f"Congratilations on company {company.name} on making a match with professional with username {professional_username}"
    else:
        return response
