from fastapi import APIRouter, File, Depends
from common.responses import BadRequest
from data.models.common_models import CreatedLocation
from data.models.professional_models import (
    CreatedProfessional,
    Professional,
    ResponceModelProfessional,
)
from services import professionals_service, match_request_service
from services.admins_service import get_id_from_token

professionals_router = APIRouter(prefix="/professionals")


@professionals_router.post("/register")
def register(professional: CreatedProfessional, location: CreatedLocation):
    professional_or_error = professionals_service.create(professional, location)
    if isinstance(professional_or_error, Professional):
        return professional_or_error
    else:
        return BadRequest(content=f"{professional_or_error}")


@professionals_router.get("")
def get_all_professionals():
    professionals = professionals_service.all(get_professional_data_func=None)

    return professionals


@professionals_router.get("/{username}")
def get_professional_by_username(username: str):
    professional = professionals_service.find_by_username(username, get_data_func=None)

    return professional


@professionals_router.post("/photo/{professional_id}")
def uploaded_photo(professional_id: int, file: bytes | None = File(default=None)):
    if not file:
        return {"message": "No file sent"}
    else:
        uploaded_photo = professionals_service.upload_photo(file, professional_id)
        if uploaded_photo:
            return f"Your photo was successfully updated"


@professionals_router.put("/{professional_id}")
def edit(
    old: ResponceModelProfessional,
    new: ResponceModelProfessional,
    professional_id,
    update_professional_data_func=None,
):
    professional = professionals_service.edit_professional_info(
        old, new, professional_id, update_professional_data_func=None
    )

    return professional


@professionals_router.get("/match_requests/received")
def view_received_match_requests(
    cv_ad_id: int | None = None,
    professional_id: int = Depends(get_id_from_token),
):
    professional = professionals_service.find_by_id(professional_id)
    if not professional:
        return BadRequest(f"Professional with id:{professional_id} does not exist")
    else:
        return match_request_service.view_match_requests_sent_to_professional(
            professional_id, cv_ad_id
        )


@professionals_router.get("/match_requests/sent")
def view_sent_match_requests(
    cv_ad_id: int | None = None,
    professional_id: int = Depends(get_id_from_token),
):
    professional = professionals_service.find_by_id(professional_id)
    if not professional:
        return BadRequest(f"Professional with id:{professional_id} does not exist")
    else:
        return match_request_service.view_match_requests_sent_by_professional(
            professional_id, cv_ad_id
        )


@professionals_router.get("/match_requests/confirm/ad_to_cv/{match_request_id}")
def confirm_match_request_ad_to_cv(
    match_request_id: int | None = None,
    professional_id: int = Depends(get_id_from_token),
):
    professional = professionals_service.find_by_id(professional_id)
    if not professional:
        return BadRequest(f"Professional with id:{professional_id} does not exist")

    response = match_request_service.professional_confirm_match_request_ad_to_cv(
        professional_id, match_request_id
    )
    if response == "Invalid match request":
        return BadRequest(
            f"Match request with id:{match_request_id} does not belong to professional with id:{professional_id}"
        )
    elif type(response) == dict:
        company_name = response["company"]
        job_ad_id = response["job_ad_id"]
        return f"Congratilations on professional {professional.username} on making a match with comapny {company_name} on its job_ad with id:{job_ad_id}"
    else:
        return response


@professionals_router.get("/match_requests/confirm/company_to_cv/{match_request_id}")
def confirm_match_request_company_to_cv(
    match_request_id: int | None = None,
    professional_id: int = Depends(get_id_from_token),
):
    professional = professionals_service.find_by_id(professional_id)
    if not professional:
        return BadRequest(f"Professional with id:{professional_id} does not exist")

    response = match_request_service.professional_confirm_match_request_company_to_cv(
        professional_id, match_request_id
    )
    if response == "Invalid match request":
        return BadRequest(
            f"Match request with id:{match_request_id} does not belong to professional with id:{professional_id}"
        )
    elif type(response) == dict:
        company_name = response["company"]
        return f"Congratulations on professional {professional.username} on making a match with comapny {company_name}"
    else:
        return response


@professionals_router.post("/match_requests/job_ad/{jobad_id}")
def send_match_request_to_job_ad(
    jobad_id: int,
    professional_id: int = Depends(get_id_from_token),
):
    professional = professionals_service.find_by_id(professional_id)
    if not professional:
        return BadRequest(f"Professional with id:{professional_id} does not exist")
    else:
        response = match_request_service.match_request_professional_to_jobad(
            professional_id, jobad_id, professional.username
        )
        if response == 1:
            return (
                f"Match request successfully sent by professional {professional.first_name} {professional.last_name} to "
                f"Job ad with id:{jobad_id} "
            )


@professionals_router.post("/match_requests/cv_ad_to_job_ad/{cv_ad_id}/{job_ad_id}")
def send_match_request_to_job_ad_via_cv_ad(
    job_ad_id: int,
    cv_ad_id: int,
    professional_id: int = Depends(get_id_from_token),
):
    professional = professionals_service.find_by_id(professional_id)
    if not professional:
        return BadRequest(f"Company with id:{professional_id} does not exist")
    else:
        response = match_request_service.match_request_professional_cvad_to_jobad(
            cv_ad_id, job_ad_id, professional.username
        )
        if response == 1:
            return (
                f"Match request successfully sent by professional {professional.first_name} {professional.last_name}"
                f" with cv_ad:{cv_ad_id} to job ad with id:{job_ad_id} "
            )
