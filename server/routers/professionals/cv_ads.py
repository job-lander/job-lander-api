from fastapi import APIRouter, Depends
from common.responses import NotFound
from common.security import validate_token
from data.models.common_models import CreatedLocation, CreatedSkill
from data.models.professional_models import CreatedCVAd, CVAdResponseModel
from services import cv_ads_service

from services.admins_service import get_id_from_token

CVs_router = APIRouter(prefix="/CV_ads")


@CVs_router.post("/")
def create(
    cv_ad: CreatedCVAd,
    location: CreatedLocation,
    skills: list[CreatedSkill],
    owner_id: int = Depends(get_id_from_token),
):
    professional_id = owner_id
    cv_ad = cv_ads_service.create_cv_ad(cv_ad, location, skills, professional_id)

    return cv_ad


@CVs_router.get("/professional/{professional_id}")
def get_all_cv_ads_by_professional(professional_id: int):
    cv_ads = cv_ads_service.all_ads_by_id(professional_id, get_data_func=None)

    return cv_ads


@CVs_router.get("/{cv_ad_id}")
def find_cv_ad_by_id(cv_ad_id: int):
    cv_ad = cv_ads_service.find_cv_by_id(cv_ad_id, get_data_func=None)

    return cv_ad


@CVs_router.post("/{cv_ad_id}/skills")
def add_skill_to_cv_ad(skill: CreatedSkill, cv_ad_id: int):
    cv_ad = cv_ads_service.find_cv_by_id(cv_ad_id)

    updated_cv_ad = cv_ads_service.add_skill_to_ad(skill, cv_ad_id, cv_ad)

    return f"Skill {skill.name} successfully added to job_ad with id {cv_ad_id}"


@CVs_router.get("/")
def get_by_criteria(
    title: str | None = None,
    short_desc: str | None = None,
    salary_low: int | None = None,
    salary_high: int | None = None,
    salary_treshold: int | None = None,  # whole number for percentage. Eg. 20 for 20%
    is_remote: int | None = None,
    professional_id: int | None = None,
    town: str | None = None,
    country: str | None = None,
    skills: list[CreatedSkill] | None = None,
    skills_number_treshold: int = 0,  # ability to lower the matching critedia by number of skills
    authorized=Depends(validate_token),
) -> list[CVAdResponseModel]:
    cv_ad = cv_ads_service.find_by_criteria(
        title,
        short_desc,
        salary_low,
        salary_high,
        salary_treshold,
        professional_id,
        skills,
        skills_number_treshold,
        is_remote,
        town,
        country,
    )
    if not cv_ad:
        return NotFound()
    return cv_ad
