from fastapi import APIRouter, Depends, HTTPException

from data.models.common_models import Status

from services.admins_service import check_if_admin
from services.companies_service import find_by_id, change_company_status

companies_admins_router = APIRouter(prefix="/admins/companies")


@companies_admins_router.put("/approve")
def approve_registration(company_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	
	company = find_by_id(company_id)
	if not company:
		raise HTTPException(status_code=404, detail="Company not found!")
	company.id = change_company_status(company.id, Status.APPROVED)
	
	return {'message': f'Company {company.id} is approved'}


@companies_admins_router.put("/block")
def block_company(company_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	
	company = find_by_id(company_id)
	if not company:
		raise HTTPException(status_code=404, detail="Company not found!")
	
	company.id = change_company_status(company.id, Status.BLOCKED)
	
	return {'message': f'Company {company.id} is blocked'}


@companies_admins_router.put("/unblock")
def unblock_company(company_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	
	company = find_by_id(company_id)
	if not company:
		raise HTTPException(status_code=404, detail="Company not found!")
	company.id = change_company_status(company.id, Status.APPROVED)
	
	return {'message': f'Company {company.id} is unblocked and approved again!'}


@companies_admins_router.put("/delete")
def delete_company(company_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	
	company = find_by_id(company_id)
	if not company:
		raise HTTPException(status_code=404, detail="Company not found!")
	
	company.id = change_company_status(company.id, Status.FOR_DELETION)
	
	return {'message': f'Company {company.id} is deleted'}
