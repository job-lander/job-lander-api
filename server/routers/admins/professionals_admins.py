from fastapi import APIRouter, Depends
from fastapi import HTTPException

from data.models.common_models import Message, Status

from services.admins_service import check_if_admin
from services.professionals_service import exist, change_professional_status

professionals_admins_router = APIRouter(prefix="/admins/professionals")


@professionals_admins_router.put("/approve", response_model=Message)
def approve_registration(professional_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	if not exist(professional_id):
		raise HTTPException(status_code=404, detail='Professional not found')
	professional_id = change_professional_status(professional_id, Status.APPROVED)
	
	message = Message(content=f'Professional {professional_id} is approved')
	return message


@professionals_admins_router.put("/block", response_model=Message)
def block_professional(professional_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	if not exist(professional_id):
		raise HTTPException(status_code=404, detail='Professional not found')
	professional_id = change_professional_status(professional_id, Status.BLOCKED)
	
	message = Message(content=f'Professional {professional_id} is blocked!')
	return message


@professionals_admins_router.put("/unblock", response_model=Message)
def unblock_professional(professional_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	if not exist(professional_id):
		raise HTTPException(status_code=404, detail='Professional not found')
	professional_id = change_professional_status(professional_id, Status.APPROVED)
	
	message = Message(content=f'Professional {professional_id} is unblocked and approved again!')
	return message


@professionals_admins_router.put("/delete", response_model=Message)
def delete_professional(professional_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	if not exist(professional_id):
		raise HTTPException(status_code=404, detail='Professional not found')
	professional_id = change_professional_status(professional_id, Status.FOR_DELETION)
	
	message = Message(content=f'Professional {professional_id} is deleted!')
	return message
