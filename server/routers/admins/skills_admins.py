from fastapi import APIRouter, Depends, HTTPException

from data.models.common_models import Message, Status, SkillIn

from services.admins_service import check_if_admin
from services.skill_service import create, exist, change_skill_status

skills_admins_router = APIRouter(prefix="/admins/skills")


@skills_admins_router.post("/", response_model=Message)
def add_skill(skill_in: SkillIn, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")

	skill = create(skill_in.name)
	skill_id = change_skill_status(skill.id, Status.APPROVED)
	
	message = Message(content=f'Skill with id:{skill_id} and name:{skill.name} is added')
	return message


@skills_admins_router.put("/", response_model=Message)
def approve_skill(skill_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	if not exist(skill_id):
		raise HTTPException(status_code=404, detail='Skill not found')
	skill_id = change_skill_status(skill_id, Status.APPROVED)
	
	message = Message(content=f'Skill {skill_id} is approved')
	return message


@skills_admins_router.put("/delete", response_model=Message)
def delete_skill(skill_id: int, is_admin=Depends(check_if_admin)):
	if not is_admin:
		raise HTTPException(status_code=403, detail="Unauthorized!")
	if not exist(skill_id):
		raise HTTPException(status_code=404, detail='Skill not found')
	skill_id = change_skill_status(skill_id, Status.FOR_DELETION)
	
	message = Message(content=f'Skill {skill_id} is deleted!')
	return message
