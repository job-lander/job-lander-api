from fastapi import APIRouter
from data.models.admin_models import AdminIn, AdminOut
from services import admins_service
from common.responses import BadRequest

admins_router = APIRouter(prefix='/admins')


@admins_router.post('/register', response_model=AdminOut)
def register_admin(admin: AdminIn):
	admin_id = admins_service.create_admin(admin)
	
	return AdminOut(
		id=admin_id, username=admin.username,
		first_name=admin.first_name,
		last_name=admin.last_name,
		email=admin.email
		) if admin_id else BadRequest("Username or email taken!")
