from mariadb import connect
from mariadb.connections import Connection
from common.config import settings


def _get_connection() -> Connection:
    return connect(
        user=settings.DB_USER,
        password=settings.DB_PASSWORD,
        host=settings.DB_HOST,
        port=settings.DB_PORT,
        database=settings.DB_DATABASE,
    )


def read_query(sql: str, sql_params=()):
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return list(cursor)


def read_query_transacional(sql: str, conn: Connection, sql_params=()):
    cursor = conn.cursor()
    cursor.execute(sql, sql_params)

    return list(cursor)


def read_query_to_dict(sql: str, sql_params=()):
    with _get_connection() as conn:
        cursor = conn.cursor(dictionary=True)
        cursor.execute(sql, sql_params)

        return cursor.fetchone()


def insert_query(sql: str, sql_params=()) -> int:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.lastrowid


def insert_query_transactional(sql: str, conn: Connection, sql_params=()) -> int:
    cursor = conn.cursor()
    cursor.execute(sql, sql_params)

    return cursor.lastrowid


def update_query(sql: str, sql_params=()) -> bool:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.rowcount


def update_query_transactional(sql: str, conn: Connection, sql_params=()) -> bool:
    cursor = conn.cursor()
    cursor.execute(sql, sql_params)

    return cursor.rowcount


def delete_query(sql: str, sql_params=()):
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()
