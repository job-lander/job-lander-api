from pydantic import BaseModel

from data.models.common_models import CreatedSkill


class Company(BaseModel):
    id: int
    username: str
    name: str
    password: str
    description: str
    UIC: int | None
    website: str | None
    twitter: str | None
    logo_id: int
    status_id: int

    @classmethod
    def from_query_result(
        cls,
        id,
        username,
        name,
        password,
        description,
        UIC,
        website,
        twitter,
        logo_id,
        status_id,
    ):
        return cls(
            id=id,
            username=username,
            name=name,
            password=password,
            description=description,
            UIC=UIC,
            website=website,
            twitter=twitter,
            logo_id=logo_id,
            status_id=status_id,
        )


class CreatedCompany(BaseModel):
    username: str
    name: str
    password: str
    description: str
    UIC: str | None
    website: str | None
    twitter: str | None


class CompanyContacts(BaseModel):
    id: int
    town: str
    country: str
    address: str | None
    phone: str | None
    email: str

    @classmethod
    def from_query_result(
        cls,
        id: int,
        town: str,
        country: str,
        address: str | None,
        phone: str | None,
        email: str,
    ):
        return cls(
            id=id, town=town, country=country, address=address, phone=phone, email=email
        )


class CompanyInfo(BaseModel):
    id: int
    name: str
    description: str
    UIC: int | None
    website: str
    twitter: str | None
    logo: bytes
    active_job_ads: int
    number_of_successful_matches: int
    contacts: list[CompanyContacts]


class Contacts(BaseModel):
    id: int
    address: str | None
    phone: str | None
    email: str
    location_id: int
    company_id: int

    @classmethod
    def from_query_result(cls, id, address, phone, email, location_id, company_id):
        return cls(
            id=id,
            address=address,
            phone=phone,
            email=email,
            location_id=location_id,
            company_id=company_id,
        )


class CreatedContacts(BaseModel):
    address: str | None
    phone: str | None
    email: str


class JobAd(BaseModel):
    id: int
    title: str
    description: str
    salary_low: int
    salary_high: int
    is_remote: int
    company_id: int
    location_id: int
    status_id: int


class JobAdResponseModel(BaseModel):
    id: int
    title: str
    description: str
    company_id: int
    salary_low: int
    salary_high: int
    is_remote: int
    status_id: int
    town: str
    country: str
    skills: list[CreatedSkill] = []

    @classmethod
    def from_query_result(
        cls,
        id,
        title,
        description,
        company_id,
        salary_low,
        salary_high,
        is_remote,
        status_id,
        town,
        country,
        skills=[],
    ):
        return cls(
            id=id,
            title=title,
            description=description,
            company_id=company_id,
            salary_low=salary_low,
            salary_high=salary_high,
            is_remote=is_remote,
            status_id=status_id,
            town=town,
            country=country,
            skills=skills,
        )


class CreatedJobAd(BaseModel):
    title: str
    description: str
    salary_low: int
    salary_high: int
    is_remote: int
