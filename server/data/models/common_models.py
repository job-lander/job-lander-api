from pydantic import BaseModel, constr

TUsername = constr(regex="^\w{2,30}$")
TPassword = constr(
    regex="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
)  # Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
TEmail = constr(
    regex="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}"
)  # Regex for e-mail syntax


class Location(BaseModel):
    id: int
    town_id: int
    country_id: int


class CreatedLocation(BaseModel):
    town: str
    country: str


class Town(BaseModel):
    id: int
    name: str


class Country(BaseModel):
    id: int
    name: str


class Status:
    ACTIVE = 1
    ARCHIVED = 3
    BLOCKED = 6
    DELETED = 7
    FOR_DELETION = 8
    INACTIVE = 2
    MATCHED = 5
    PRIVATE = 4
    UNAPPROVED = 9
    APPROVED = 10
    BUSY = 11


class SkillIn(BaseModel):
    name: str


class Skill(BaseModel):
    id: int
    name: str
    status_id: int

    @classmethod
    def from_query_result(cls, id: int, name: str, status_id: int):
        return cls(id=id, name=name, status_id=status_id)


class CreatedSkill(BaseModel):
    name: str
    level: str

    @classmethod
    def from_query_result(cls, name, level):
        return cls(name=name, level=level)


class CVSkill(BaseModel):
    CV_ad_id: int
    skill_id: int
    skill_level_id: int


class JobSkill(BaseModel):
    job_ad_id: int
    skill_id: int
    skill_level_id: int


class Level(BaseModel):
    id: int
    proficiency_level: str


class Image(BaseModel):
    id: int | None
    file: bytes


class LogIn(BaseModel):
    username: TUsername
    password: TPassword


class Message(BaseModel):
    content: str
