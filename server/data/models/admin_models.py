from pydantic import BaseModel, constr

TUsername = constr(regex="^\w{2,30}$")
TPassword = constr(
    regex="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
)  # Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
TEmail = constr(
    regex="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}"
)  # Regex for e-mail syntax


class Admin(BaseModel):
    id: int
    first_name: str
    last_name: str
    username: TUsername
    password: TPassword
    email: TEmail


class AdminIn(BaseModel):
    username: TUsername
    password: TPassword
    first_name: str
    last_name: str
    email: TEmail


class AdminOut(BaseModel):
    id: int
    username: TUsername
    first_name: str
    last_name: str
    email: TEmail


class AdminFromDB(BaseModel):
    id: int
    username: str
    password: str
    first_name: str
    last_name: str
    email: str

    @classmethod
    def from_db(
        cls,
        id: int,
        username: str,
        password: str,
        first_name: str,
        last_name: str,
        email: str,
    ):
        return cls(
            id=id,
            username=username,
            password=password,
            first_name=first_name,
            last_name=last_name,
            email=email,
        )

