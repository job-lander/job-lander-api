from pydantic import BaseModel

from data.models.common_models import CreatedSkill


class Professional(BaseModel):
    id: int
    first_name: str
    last_name: str
    username: str
    password: str
    email: str
    brief_summary: str
    location_id: int
    status_id: int
    photo_id: int

    @classmethod
    def from_query_result(
        cls,
        id: int,
        first_name: str,
        last_name: str,
        username: str,
        password: str,
        email: str,
        brief_summary: str,
        location_id: int,
        status_id: int,
        photo_id: int,
    ):
        return cls(
            id=id,
            first_name=first_name,
            last_name=last_name,
            username=username,
            password=password,
            email=email,
            brief_summary=brief_summary,
            location_id=location_id,
            status_id=status_id,
            photo_id=photo_id,
        )


class ResponceModelProfessional(BaseModel):
    id: int
    first_name: str
    last_name: str
    username: str
    password: str
    email: str
    brief_summary: str
    town: str
    country: str

    @classmethod
    def from_query_result(
        cls,
        id: int,
        first_name: str,
        last_name: str,
        username: str,
        password: str,
            email: str,
            brief_summary: str,
            town: str,
            country: str,
    ):
        return cls(
            id=id,
            first_name=first_name,
            last_name=last_name,
            username=username,
            password=password,
            email=email,
            brief_summary=brief_summary,
            town=town,
            country=country,
        )


class CreatedProfessional(BaseModel):
    first_name: str
    last_name: str
    username: str
    password: str
    email: str
    brief_summary: str


class CvAd(BaseModel):
    id: int
    title: str
    short_desc: str
    salary_low: int
    salary_high: int
    is_remote: bool
    professional_id: int
    status_id: int
    locations_id: int

    @classmethod
    def from_query_result(
        cls,
        id: int,
        title: str,
        short_desc: str,
        salary_low: int,
        salary_high: int,
        is_remote: bool,
        professional_id: int,
        status_id: int,
        locations_id: int,
    ):
        return cls(
            id=id,
            title=title,
            short_desc=short_desc,
            salary_low=salary_low,
            salary_high=salary_high,
            is_remote=is_remote,
            professional_id=professional_id,
            status_id=status_id,
            locations_id=locations_id,
        )


class CVAdResponseModel(BaseModel):
    id: int
    title: str
    short_desc: str
    salary_low: int
    salary_high: int
    is_remote: int
    professional_id: int
    town: str
    country: str
    skills: list[CreatedSkill] = []

    @classmethod
    def from_query_result(
        cls,
        id,
        title,
        short_desc,
        salary_low,
        salary_high,
        is_remote,
        professional_id,
        town,
        country,
        skills=[],
    ):
        return cls(
            id=id,
            title=title,
            short_desc=short_desc,
            salary_low=salary_low,
            salary_high=salary_high,
            is_remote=is_remote,
            professional_id=professional_id,
            town=town,
            country=country,
            skills=skills,
        )


class CreatedCVAd(BaseModel):
    title: str
    short_desc: str
    salary_low: int
    salary_high: int
    is_remote: bool
