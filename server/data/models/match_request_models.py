from pydantic import BaseModel


class MRCompanySent_toCVad(BaseModel):
    match_request_id: int
    company_id: int
    company_name: str
    cv_ad_id: int
    professional_username: str

    @classmethod
    def from_query_result(
        cls, match_request_id, company_id, company_name, cv_ad_id, professional_username
    ):
        return cls(
            match_request_id=match_request_id,
            company_id=company_id,
            company_name=company_name,
            cv_ad_id=cv_ad_id,
            professional_username=professional_username,
        )


class MRCompanySentOrReceived_JobAdtoCVad(BaseModel):
    match_request_id: int
    company_id: int
    company_name: str
    job_ad_id: int
    cv_ad_id: int
    professional_username: str

    @classmethod
    def from_query_result(
        cls,
        match_request_id,
        company_id,
        company_name,
        job_ad_id,
        cv_ad_id,
        professional_username,
    ):
        return cls(
            match_request_id=match_request_id,
            company_id=company_id,
            company_name=company_name,
            job_ad_id=job_ad_id,
            cv_ad_id=cv_ad_id,
            professional_username=professional_username,
        )


class MRCompanyReceived_toJobAd(BaseModel):
    match_request_id: int
    company_id: int
    company_name: str
    job_ad_id: int
    professional_username: str

    @classmethod
    def from_query_result(
        cls,
        match_request_id,
        company_id,
        company_name,
        job_ad_id,
        professional_username,
    ):
        return cls(
            match_request_id=match_request_id,
            company_id=company_id,
            company_name=company_name,
            job_ad_id=job_ad_id,
            professional_username=professional_username,
        )


class MRequestProfessionalSent_toJobAd(BaseModel):
    match_request_id: int
    professional_id: int
    professional_username: str
    job_ad_id: int
    company_name: str

    @classmethod
    def from_query_result(
        cls,
        match_request_id,
        professional_id,
        professional_username,
        job_ad_id,
        company_name,
    ):
        return cls(
            match_request_id=match_request_id,
            professional_id=professional_id,
            professional_username=professional_username,
            job_ad_id=job_ad_id,
            company_name=company_name,
        )


class MRProfessionalSentOrReceived_CVAdtoJobad(BaseModel):
    match_request_id: int
    professional_id: int
    professional_username: str
    cv_ad_id: int
    job_ad_id: int
    company_name: str

    @classmethod
    def from_query_result(
        cls,
        match_request_id,
        professional_id,
        professional_username,
        cv_ad_id,
        job_ad_id,
        company_name,
    ):
        return cls(
            match_request_id=match_request_id,
            professional_id=professional_id,
            professional_username=professional_username,
            cv_ad_id=cv_ad_id,
            job_ad_id=job_ad_id,
            company_name=company_name,
        )


class MRequestProfessionalReceived_toCVAd(BaseModel):
    match_request_id: int
    professional_id: int
    professional_username: str
    cv_ad_id: int
    company_name: str

    @classmethod
    def from_query_result(
        cls,
        match_request_id,
        professional_id,
        professional_username,
        cv_ad_id,
        company_name,
    ):
        return cls(
            match_request_id=match_request_id,
            professional_id=professional_id,
            professional_username=professional_username,
            cv_ad_id=cv_ad_id,
            company_name=company_name,
        )
