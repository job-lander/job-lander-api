import secrets
from pydantic import BaseSettings


class Settings(BaseSettings):
    API_STR: str = "/api"
    BASE_URL: str = "http://127.0.0.1:8000"

    # Security and token
    SECRET_KEY: str = secrets.token_urlsafe()
    # 60 minutes * 24 hours * 8 days = 8 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8
    # # 1 minute
    # ACCESS_TOKEN_EXPIRE_MINUTES: int = 1
    ALGORITHM = "HS256"

    # Data Base credentials
    DB_USER: str
    DB_PASSWORD: str
    DB_HOST: str
    DB_PORT: int
    DB_DATABASE: str

    # User specifics
    USERS_OPEN_REGISTRATION: bool = False

    # Email API keys
    EMAIL_API_KEY: str
    EMAIL_API_SECRET: str


settings = Settings(_env_file=".env")
