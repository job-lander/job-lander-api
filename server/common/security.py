from datetime import datetime, timedelta
from fastapi import Header

import jwt
from jwt import PyJWTError
from passlib.context import CryptContext

from common.config import settings
from fastapi.exceptions import HTTPException

from data.database import read_query
from data.models.company_models import Company, JobAd
from data.models.professional_models import Professional, CvAd

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def create_access_token(subject: str | dict, expires_delta: timedelta = None) -> str:
	if expires_delta:
		expire = datetime.utcnow() + expires_delta
	else:
		expire = datetime.utcnow() + timedelta(
			minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
		)
	to_encode = {"exp": expire, "sub": subject}
	encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
	return encoded_jwt


def decode_access_token(token: str):
	try:
		payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
	except PyJWTError:
		raise HTTPException(
			status_code=401,
			detail="Could not validate credentials",
		)
	return payload


def verify_password(plain_password: str, hashed_password: str) -> bool:
	return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
	return pwd_context.hash(password)


def check_if_admin(token: str, read_func=None):
	if read_func is None:
		read_func = read_query
	
	payload_token = decode_access_token(token)
	from_token_id = payload_token["sub"]["id"]
	
	result = read_func(
		"""SELECT 1 FROM job_lander.admin AS a WHERE a.id = ?""", (from_token_id,)
	)
	if result:
		return True
	raise HTTPException(status_code=403)


def validate_token(token: str = Header()):
	decode_access_token(token)


def check_if_owner(token: str, entity: Company | Professional | CvAd | JobAd) -> bool:
	payload_token = decode_access_token(token)
	from_token_id = payload_token["sub"]["id"]
	
	if isinstance(entity, Company):
		return from_token_id == entity.id
	elif isinstance(entity, Professional):
		return from_token_id == entity.id
	elif isinstance(entity, CvAd):
		return from_token_id == entity.professional_id
	elif isinstance(entity, JobAd):
		return from_token_id == entity.company_id
