<img src="https://i.postimg.cc/jq1h10JT/Joblander-wallpaper-2-1024-50.jpg" alt="Job_Lander_logo" width="1024px" style="margin-top: 20px;"/>


# WEB - Group 8 - Job Lander

| |Team Members |
|--|--|
|🍓| [Rossitsa Racheva](https://gitlab.com/rossitsa.racheva) 
|🍒| [Nikolay Likyov](https://gitlab.com/niki.likiov) |
|🍉| [Evgeni Vladimirov](https://gitlab.com/evvladimirov) |


> Equilibrium is only achieved with careful balancing of all mechanism parts

## [](#project-description)Project Description

Design and implement a **Job seeking and matching platform** and provide **RESTful API** that can be consumed by different clients. High-level description:

-  Professionals and Companies register profiles and post job ads and CVs
- Users search for a suitable match via an advanced search functions
- Afterwards they send match requests that can be matched by the other side
-  Administrators manage profiles, ads, skills and platform order

## [](#technology-description)Technology description

The **Job Lander platform** is developed on Python v3.10.5 following all **RESTful API** requirements and best practices (that we know so far).

-   The program us run via the `uvicorn main:app` command called from the server folder, or simply double clicking the `Run Server.bat` file;
-   A globally accessible **MariaDB MySQL** database is used hosted on a remote private server. Data export as .sql file is also available ;
-   **FastAPI** is the used framework for development, that uses the **Pydantic** library for data validation based on the standard Python type hints;
-   The code is complete with **DI** complemented with **Unit Tests**;
-   The **Postman** tool was used for functionality testing with a shared library where each team member could upload/view/test the various commands. Some of the commands were enhanced using Postman Visualizer with HTML code. The complete command library is exported as a JSON file in the main directory in GitLab - Job_Lander.postman_collection.json; It is also available via [this link](https://red-crescent-74496.postman.co/workspace/WEB-Team-Project---Superstar-Fo~1e7977a7-2c8a-4e9b-9fcf-5cab73476406/collection/23177946-5e7b06a9-8ad6-4982-8ed4-1f0b33e2ef31?action=share&creator=23170333);
-   The program was developed with the use of **GitLab** version control system using branches, issue boards tasks and careful commit curation;
-   An automatically generated Swagger endpoint and method library is available at `localhost:8000/docs` on running server;
-   A database relation screenshot is available in the git main directory;
-   A complete list of all project dependencies is available described in the `requrements.txt` file;

The business logic is separated from the technological specifics using different layers and thus achieving separation of concerns:

 -   **data/models** - module, defines the objects structure;
 -   **data/database** - module with the database access data as well as all different types of database queries used;
 -   **routers layer** - the layer which defines the endpoints along with the specific HTTP methods and basic validation of the commands and data;
 -   **services layer** - the layer in which the buysiness logic is defined - the object commands as well as specifics of the database queries.

### [](#Advanced-security_features)Advanced security features
 - program specific credentials are stored in an `.env` file which is not synced with git via the **gitignore** method;
 - **passlib** library used for token creation and password hash;
 - password hashes are different every time as **dynamic salt** is added to any password string upon hashing;
 - tokens are valid only for 8 days and per server session - different for every server session;

## [](#implemented-restful-api-features)Implemented RESTful API features

### [](#comapnies_admins_endpoints)_Admin commands for companies_

##### [](#approve-registration)Registration approval

-   Accepts company registrations and approves the profiles.
-   Requires **admin** authentication.

##### [](#block-company)Blocks company

-   Blocks companies.
-   Requires **admin** authentication.

##### [](#unblock-company)Unblocks company

-   Unblocks companies.
-   Requires **admin** authentication.

##### [](#delete-company)Delete 

-   Responcible for the deletion of companies.
-   Requires **admin** authentication.

### [](#professionals_admins_endpoints)_Admin commands for professionals_

##### [](#approve-registration)Registration approval

-   Accepts professionals registrations and approves the profiles.
-   Requires **admin** authentication.

##### [](#block-professional)Blocks professional

-   Blocks professionals.
-   Requires **admin** authentication.

##### [](#unblock-professional)Unblocks professional

-   Unblocks professionals.
-   Requires **admin** authentication.

##### [](#delete-company)Delete 

-   Responcible for the deletion of professionals.
-   Requires **admin** authentication.

### [](#skills_admins)_Admin commands for skills_

##### [](#add-skill)Add skill

-   Adds authomatically approved skills. 
-   Requires **admin** authentication.

##### [](#approve-skill)Skill approval

-   Approves skills requested by professionals and companies.
-   Requires **admin** authentication.

##### [](#delete-skill)Delete 

-   Responcible for the deletion of skills.
-   Requires **admin** authentication.

### [](#login_endpoints)_Auth - login_

##### [](#login-prof)Professional login

-   Logs professionals in.
-   Returns authentication token.

##### [](#login-comp)Company login

-   Logs companies in.
-   Returns authentication token.

##### [](#login-admin)Admin login

-   Logs admins in.
-   Returns authentication token.

##### [](#test-verify-token)Vierifies given tokens

-   Decodes and verifies given token.

### [](#companies_endoints)_Company Commands_

##### [](#register)Register Company

-   Registers a new company.
-   Creates contacts for this company.
-   Creates location ID based on the town and the country where the company branch is.

##### [](#add-contacts)Add contacts

-   Requires authentication.
-   Adds new contact information for another branch of the company
-   Creates or adds new location for the new branch of the company

##### [](#update-contacts)Update contacts

-   Requires authentication.
-   Updates contact information of the company.

##### [](#view-company-info)View company information

-   Shows company information of a company by given ID

##### [](#upload-logo)Upload Logo

-   Adds an image to the company's info and shows its size.
-   The image is visible in the DB.
-   Requires authentication.

##### [](#edit-company-info)Edit company information 

-   Updates company information.
-   Requires authentication.

##### [](#send-match-request-to-cvad)Sending match requests to CV ad

-   Sends a match request to a professional regarding his CV ad.
-   Requires authentication.

##### [](#send-match-request-to-cvad-via-job-ad)Sending match requests to CV ad from Job ad

-   Sends a match request to a professional regarding his CV ad matching it to company's Job ad.
-   Requires authentication.

##### [](#view-recieved-match-request)View recieved match requested

-   Shows all match requests that the company recieved from professionals.
-   Requires authentication.

##### [](#view-sent-match-request)View sent match requested

-   Shows all match requests that the company sent to professional's CV ads.
-   Requires authentication.

##### [](#confirm-match-request-professional-to-ad)Confirm match request from professional

-   Confirms a match requests that professionals sent to Job ads.
-   Requires authentication.

##### [](#confirm-match-request-cv-to)Confirm match request from professional's CV ad

-   Confirms a match requests that were recieved from professional's CV ads to Job ads.
-   Requires authentication.

### [](#job-ads-endpoints)_Job ads_
 
##### [](#create)Create job ads

-   Creates a new job ad
-   Sends for approval unapproved skills that are added to the ad.
-   Requires authentication.

##### [](#all-job-ads-by-company)View job ads

-   Shows all job ads the company has and their status. 
-   Requires authentication.

##### [](#find-job-ad-by-id)Find job ad by id

-   Shows specific job ad found by it's id. 
-   Requires authentication.

##### [](#make-job-ad-active)Activate job ad

-   Changes the status of the ad to active. 
-   Requires authentication.

##### [](#edit-job-ad-by-id)Updates job ad

-   Updates the job ad.
-   Requires authentication.

##### [](#edit-job-ad-by-id)Updates job ad

-   Adds skill to job ad.
-   Requires authentication.

##### [](#get-by-criteria)Searches job ad by criteria

-   Searches for a job ad by: title, description, salary, company id, skills, location.
-   Can find a job id by specific number of skills.
-   Can find a job ad by specific salary range. 
-   Requires authentication.

### [](#professionals_endoints)_Professional Commands_

##### [](#register)Register Professional

-   Registers a new professional.
-   Creates location ID based on the town and the country where the professional resides.

##### [](#get-all-professionals)View all professionals

-   Shows all professionals and information about them.
-   Requires authentication.

##### [](#get-professional-by-username)Look up a professional by id

-   Shows a professional found by username.
-   Requires authentication.

##### [](#upload-photo)Upload Photo

-   Adds an image to the professional's info and shows its size.
-   The image is visible in the DB.
-   Requires authentication.


##### [](#edit)Edit professional information 

-   Updates professional information.
-   Requires authentication.

##### [](#send-match-request-to-job-ad)Sending match requests to Job ad

-   Sends a match request to a professional regarding his Job ad.
-   Requires authentication.

##### [](#send-match-request-to-job-ad-via-CV-ad)Sending match requests to CV ad from Job ad

-   Sends a match request to a professional regarding his Job ad matching it to company's CV ad.
-   Requires authentication.

##### [](#view-recieved-match-request)View recieved match requested

-   Shows all match requests that the professional recieved from professionals.
-   Requires authentication.

##### [](#view-sent-match-request)View sent match requested

-   Shows all match requests that the professional sent to company's Job ads.
-   Requires authentication.

##### [](#confirm-match-request-company-to-cv)Confirm match request from company

-   Confirms a match requests that companies sent to CV ads.
-   Requires authentication.

##### [](#confirm-match-request-ad-to-cv)Confirm match request from company's Job ad

-   Confirms a match requests that were recieved from company's Job ads to CV ads.
-   Requires authentication.

### [](#CV-ads-endpoints)_CV ads_
 
##### [](#create)Create CV ads

-   Creates a new CV ad.
-   Sends for approval unapproved skills that are added to the ad.
-   Requires authentication.

##### [](#all-job-CV-by-company)View CV ads

-   Shows all CV ads the company has and their status. 
-   Requires authentication.

##### [](#find-CV-ad-by-id)Find CV ad by id

-   Shows specific CV ad found by it's id. 
-   Requires authentication.

##### [](#get-by-criteria)Searches CV ad by criteria

-   Searches for a CV ad by: title, description, salary, professional id, skills, location.
-   Can find a CV id by specific number of skills.
-   Can find a CV ad by specific salary range. 
-   Requires authentication.

### [](#image-endpoints)_Images_
 
##### [](#create_file)Add image

-   Adds an image to the DB.

### [](#skills-endpoints)_Skills_
 
##### [](#get-skills)View all skills

- View a list of all the approved skills.

##### [](#create-skill)Creates skill

- Creates a skill that is not yet approved.
