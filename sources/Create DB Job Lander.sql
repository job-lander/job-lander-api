-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema job_lander
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema job_lander
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `job_lander` DEFAULT CHARACTER SET utf8mb4 ;
USE `job_lander` ;

-- -----------------------------------------------------
-- Table `job_lander`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`admin` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`images`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`images` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `image` BLOB NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`statuses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`statuses` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`companies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`companies` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) CHARACTER SET 'utf8mb3' NOT NULL,
  `name` VARCHAR(50) CHARACTER SET 'utf8mb3' NOT NULL,
  `password` VARCHAR(65) NOT NULL,
  `description` VARCHAR(2500) NOT NULL,
  `logo_id` INT(11) NOT NULL DEFAULT 1,
  `status_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  INDEX `fk_Companies_images1_idx` (`logo_id` ASC) VISIBLE,
  INDEX `fk_Companies_statuses1_idx` (`status_id` ASC) VISIBLE,
  CONSTRAINT `fk_Companies_images1`
    FOREIGN KEY (`logo_id`)
    REFERENCES `job_lander`.`images` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Companies_statuses1`
    FOREIGN KEY (`status_id`)
    REFERENCES `job_lander`.`statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`countries` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(56) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 199
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`towns`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`towns` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(85) CHARACTER SET 'utf8mb3' NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 34
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`locations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`locations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `town_id` INT(11) NOT NULL,
  `country_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_locations_Towns_idx` (`town_id` ASC) VISIBLE,
  INDEX `fk_locations_Countries1_idx` (`country_id` ASC) VISIBLE,
  CONSTRAINT `fk_locations_Countries1`
    FOREIGN KEY (`country_id`)
    REFERENCES `job_lander`.`countries` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locations_Towns`
    FOREIGN KEY (`town_id`)
    REFERENCES `job_lander`.`towns` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`contacts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(200) CHARACTER SET 'utf8mb3' NULL DEFAULT 'null',
  `website` VARCHAR(100) CHARACTER SET 'utf8mb3' NULL DEFAULT 'null',
  `twitter` VARCHAR(100) NULL DEFAULT 'null',
  `phone` VARCHAR(45) NULL DEFAULT 'null',
  `email` VARCHAR(45) NOT NULL,
  `UIC` INT(10) NULL DEFAULT 0,
  `location_id` INT(11) NOT NULL,
  `company_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  UNIQUE INDEX `phone_UNIQUE` (`phone` ASC) VISIBLE,
  UNIQUE INDEX `website_UNIQUE` (`website` ASC) VISIBLE,
  UNIQUE INDEX `twitter_UNIQUE` (`twitter` ASC) VISIBLE,
  INDEX `fk_contacts_locations1_idx` (`location_id` ASC) VISIBLE,
  INDEX `fk_contacts_companies1_idx` (`company_id` ASC) VISIBLE,
  CONSTRAINT `fk_contacts_locations1`
    FOREIGN KEY (`location_id`)
    REFERENCES `job_lander`.`locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contacts_companies1`
    FOREIGN KEY (`company_id`)
    REFERENCES `job_lander`.`companies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`professionals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`professionals` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) CHARACTER SET 'utf8mb3' NOT NULL,
  `last_name` VARCHAR(50) CHARACTER SET 'utf8mb3' NOT NULL,
  `username` VARCHAR(45) CHARACTER SET 'utf8mb3' NOT NULL,
  `password` VARCHAR(65) NOT NULL,
  `brief_summary` VARCHAR(500) CHARACTER SET 'utf8mb3' NOT NULL,
  `location_id` INT(11) NOT NULL,
  `status_id` INT(11) NOT NULL,
  `photo_id` INT(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  INDEX `fk_professionals_locations1_idx` (`location_id` ASC) VISIBLE,
  INDEX `fk_professionals_statuses1_idx` (`status_id` ASC) VISIBLE,
  INDEX `fk_professionals_images1_idx` (`photo_id` ASC) VISIBLE,
  CONSTRAINT `fk_professionals_images1`
    FOREIGN KEY (`photo_id`)
    REFERENCES `job_lander`.`images` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_professionals_locations1`
    FOREIGN KEY (`location_id`)
    REFERENCES `job_lander`.`locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_professionals_statuses1`
    FOREIGN KEY (`status_id`)
    REFERENCES `job_lander`.`statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`cv_ads`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`cv_ads` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) CHARACTER SET 'utf8mb3' NOT NULL,
  `short_desc` VARCHAR(2500) CHARACTER SET 'utf8mb3' NOT NULL,
  `salary_low` INT(7) NOT NULL,
  `salary_high` INT(7) NOT NULL,
  `is_remote` TINYINT(4) NOT NULL DEFAULT 0,
  `professional_id` INT(11) NOT NULL,
  `status_id` INT(11) NOT NULL,
  `locations_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CV_ad_professionals1_idx` (`professional_id` ASC) VISIBLE,
  INDEX `fk_CV_ad_statuses1_idx` (`status_id` ASC) VISIBLE,
  INDEX `fk_CV_ad_locations1_idx` (`locations_id` ASC) VISIBLE,
  CONSTRAINT `fk_CV_ad_locations1`
    FOREIGN KEY (`locations_id`)
    REFERENCES `job_lander`.`locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CV_ad_professionals1`
    FOREIGN KEY (`professional_id`)
    REFERENCES `job_lander`.`professionals` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CV_ad_statuses1`
    FOREIGN KEY (`status_id`)
    REFERENCES `job_lander`.`statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`skills`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`skills` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) CHARACTER SET 'utf8mb3' NOT NULL,
  `status_id` INT(11) NULL DEFAULT 8,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`levels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`levels` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `proficiency_level` VARCHAR(30) CHARACTER SET 'utf8mb3' NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `proficiency_level_UNIQUE` (`proficiency_level` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`cv_skills`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`cv_skills` (
  `CV_ad_id` INT(11) NOT NULL,
  `skill_id` INT(11) NOT NULL,
  `skill_level_id` INT(11) NULL DEFAULT 1,
  PRIMARY KEY (`CV_ad_id`, `skill_id`),
  INDEX `fk_CV_ad_has_skills_skills1_idx` (`skill_id` ASC) VISIBLE,
  INDEX `fk_CV_ad_has_skills_CV_ad1_idx` (`CV_ad_id` ASC) VISIBLE,
  INDEX `fk_CV_skills_levels1_idx` (`skill_level_id` ASC) VISIBLE,
  CONSTRAINT `fk_CV_ad_has_skills_CV_ad1`
    FOREIGN KEY (`CV_ad_id`)
    REFERENCES `job_lander`.`cv_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CV_ad_has_skills_skills1`
    FOREIGN KEY (`skill_id`)
    REFERENCES `job_lander`.`skills` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CV_skills_levels1`
    FOREIGN KEY (`skill_level_id`)
    REFERENCES `job_lander`.`levels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`job_ads`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`job_ads` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) CHARACTER SET 'utf8mb3' NOT NULL,
  `description` VARCHAR(5000) CHARACTER SET 'utf8mb3' NOT NULL,
  `salay_low` INT(7) NOT NULL,
  `salary_high` INT(7) NOT NULL,
  `is_remote` TINYINT(4) NOT NULL DEFAULT 0,
  `company_id` INT(11) NOT NULL,
  `location_id` INT(11) NOT NULL,
  `status_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Job_ad_companies1_idx` (`company_id` ASC) VISIBLE,
  INDEX `fk_Job_ad_locations1_idx` (`location_id` ASC) VISIBLE,
  INDEX `fk_Job_ad_statuses1_idx` (`status_id` ASC) VISIBLE,
  CONSTRAINT `fk_Job_ad_companies1`
    FOREIGN KEY (`company_id`)
    REFERENCES `job_lander`.`companies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Job_ad_locations1`
    FOREIGN KEY (`location_id`)
    REFERENCES `job_lander`.`locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Job_ad_statuses1`
    FOREIGN KEY (`status_id`)
    REFERENCES `job_lander`.`statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`job_skills`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`job_skills` (
  `skills_id` INT(11) NOT NULL,
  `Job_ad_id` INT(11) NOT NULL,
  `skill_levels_id` INT(11) NULL DEFAULT 1,
  PRIMARY KEY (`skills_id`, `Job_ad_id`),
  INDEX `fk_skills_has_Job_ad_Job_ad1_idx` (`Job_ad_id` ASC) VISIBLE,
  INDEX `fk_skills_has_Job_ad_skills1_idx` (`skills_id` ASC) VISIBLE,
  INDEX `fk_Job_skills_levels1_idx` (`skill_levels_id` ASC) VISIBLE,
  CONSTRAINT `fk_Job_skills_levels1`
    FOREIGN KEY (`skill_levels_id`)
    REFERENCES `job_lander`.`levels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_skills_has_Job_ad_Job_ad1`
    FOREIGN KEY (`Job_ad_id`)
    REFERENCES `job_lander`.`job_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_skills_has_Job_ad_skills1`
    FOREIGN KEY (`skills_id`)
    REFERENCES `job_lander`.`skills` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`match_request_company_to_cv`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`match_request_company_to_cv` (
  `company_id` INT(11) NOT NULL,
  `cv_ad_id` INT(11) NOT NULL,
  `confirmed` TINYINT(4) NULL DEFAULT 0,
  PRIMARY KEY (`company_id`, `cv_ad_id`),
  INDEX `fk_companies_has_cv_ad_cv_ad1_idx` (`cv_ad_id` ASC) VISIBLE,
  INDEX `fk_companies_has_cv_ad_companies1_idx` (`company_id` ASC) VISIBLE,
  CONSTRAINT `fk_companies_has_cv_ad_companies1`
    FOREIGN KEY (`company_id`)
    REFERENCES `job_lander`.`companies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_companies_has_cv_ad_cv_ad1`
    FOREIGN KEY (`cv_ad_id`)
    REFERENCES `job_lander`.`cv_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`match_request_cv_to_ad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`match_request_cv_to_ad` (
  `job_ad_id` INT(11) NOT NULL,
  `cv_ad_id` INT(11) NOT NULL,
  `confirmed` TINYINT(4) NULL DEFAULT 0,
  `sent_by_company` TINYINT(4) NULL DEFAULT 0,
  PRIMARY KEY (`job_ad_id`, `cv_ad_id`),
  INDEX `fk_job_ad_has_cv_ad_cv_ad1_idx` (`cv_ad_id` ASC) VISIBLE,
  INDEX `fk_job_ad_has_cv_ad_job_ad1_idx` (`job_ad_id` ASC) VISIBLE,
  CONSTRAINT `fk_job_ad_has_cv_ad_cv_ad1`
    FOREIGN KEY (`cv_ad_id`)
    REFERENCES `job_lander`.`cv_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_ad_has_cv_ad_job_ad1`
    FOREIGN KEY (`job_ad_id`)
    REFERENCES `job_lander`.`job_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `job_lander`.`match_request_professional_to_ad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `job_lander`.`match_request_professional_to_ad` (
  `professional_id` INT(11) NOT NULL,
  `job_ad_id` INT(11) NOT NULL,
  `confirmed` TINYINT(4) NULL DEFAULT 0,
  PRIMARY KEY (`professional_id`, `job_ad_id`),
  INDEX `fk_professionals_has_job_ad_job_ad1_idx` (`job_ad_id` ASC) VISIBLE,
  INDEX `fk_professionals_has_job_ad_professionals1_idx` (`professional_id` ASC) VISIBLE,
  CONSTRAINT `fk_professionals_has_job_ad_job_ad1`
    FOREIGN KEY (`job_ad_id`)
    REFERENCES `job_lander`.`job_ads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_professionals_has_job_ad_professionals1`
    FOREIGN KEY (`professional_id`)
    REFERENCES `job_lander`.`professionals` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
